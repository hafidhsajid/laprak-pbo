# Laporan Praktikum #6 -Inheritance

## Kompetensi

1. Memahami konsep dasar inheritance atau pewarisan.	
2. Mampu membuat suatu subclass dari suatu superclass tertentu.	
3. Mampu mengimplementasikan konsep single dan multilevel inheritance.	
4. Mampu membuat objek dari suatu subclass dan melakukan pengaksesan terhadap atribut dan method baik yang dimiliki 

## Ringkasan Materi

### Pendahuluan

**Inheritance** atau **pewarisan sifat** merupakan suatu cara untuk menurunkan suatu class yang lebih umum menjadi suatu class yang lebih spesifik. Inheritance adalah salah satu ciri utama suatu bahasa program yang berorientasi pada objek. Inti dari pewarisan adalah sifat reusable dari konsep object oriented. Setiap subclass akan “mewarisi” sifat dari superclass selama bersifat protected ataupun public.


Dalam inheritance terdapat dua istilah yang sering digunakan. Kelas yang menurunkan disebut kelas dasar (base class/super class), sedangkan kelas yang diturunkan disebut kelas turunan (derived class/sub class/child class) . Di dalam Java untuk mendeklarasikan suatu class sebagai subclass dilakukan dengan cara menambahkan kata kunci extends setelah deklarasi nama class, kemudian diikuti dengan nama parent class-­‐nya. Kata kunci extends tersebut memberitahu kompiler Java bahwa kita ingin melakukan perluasan class. Berikut adalah contoh deklarasi inheritance.
```java
public class B extends  A{

....
}
```

Contoh  diatas  memberitahukan  kompiler  Java  bahwa  kita  ingin  meng-­‐extend  class  A  ke  class  B. Dengan kata lain, class B adalah subclass (class turunan) dari class A, sedangkan class A adalah parent class dari class B.
Karakteristik pada super class akan dimiliki juga oleh subclassnya. Terdapat 3 bentuk pewarisan: single inheritance, multilevel inheritance, dan multiple inheritance. Namun yang akan dibahas pada jobsheet ini adalah single inheritance dan multilevel inheritance.

1.	Single Inheritance
Single inheritance adalah Suatu class yang hanya mempunyai satu parent class. Contoh:

![](img/Pendahuluan1.PNG)

Berdasarkan Gambar 1 dapat diketahui bahwa class B merupakan subclass yang mempunyai satu parent yaitu class A sehingga disebut single inheritance.

2.	Multilevel Inheritance
Multilevel inheritance adalah Suatu subclass bisa menjadi superclass bagi class yang lain. Contoh:

![](img/Pendahuluan2.PNG)

Berdasarkan Gambar 2 diatas dapat dilihat bahwa class B merupakan subclass dari class A, sehingga dalam hal ini class A adalah superclass dan class B adalah subclass. Kemudian class B yang awalmya merupakan subclass mempunyai subclass lagi yaitu class C sehingga class B menjadi superclass dari class C, begitu juga seterunya jika class C memilki subclass lagi.

Pada class diagram, pewarisan digambarkan dengan sebuah garis tegas, dengan segitiga di ujungnya. Class yang dekat pada segitiga merupakan superclass, sedangkan class yang jauh dari segitiga merupakan subclass. Untuk membentuk sebuah subclass, keyword “extends” digunakan (lihat contoh pada sesi “Implementasi Pewarisan”). Berikut ini adalah contoh class diagram dari pewarisan:

![](img/Pendahuluan3.PNG)

Suatu  parent  class  dapat  tidak  mewariskan  sebagian  member-­‐nya  kepada  subclass-­‐nya. Sejauh mana suatu member dapat diwariskan ke  class  lain,  ataupun  suatu  member  dapat  diakses  dari class lain, sangat berhubungan dengan access control (kontrol  pengaksesan).  Di  dalam  java,  kontrol pengaksesan dapat digambarkan dalam tabel berikut ini:

![](img/Pendahuluan4.PNG)

Kata kunci super dipakai untuk merujuk pada member dari parent class. Sebagaimana kata kunci this yang dipakai untuk merujuk pada member dari class itu sendiri. Format penulisannya adalah sebagai berikut:
- `super.namaAtribut`

    Merujuk/mengakses atribut dari parent class /superclass
- `super.namaMethod()`

    Merujuk/memanggil method dari parent class /superclass
- `super()`

    Merujuk / memanggil konstruktor parent class /superclass Hanya bisa digunakan dibaris pertama dalam kontruktor.
- `super(parameter1, parameter2,dst)`

    Merujuk / memanggil konstruktor berparamter dari superklas

Ketika mmebuat objek dari subclass, pada saat itu juga objek pada superclass juga akan terbentuk. Dengan katalain, ketika kontruktor subclass dijalankan untuk membuat objek, saat itu juga kontruktor superclass akan berjalan. Jadi di setiap konstruktor subclass, pada baris pertama konstruktor subclass tersebut akan dipanggil konstruktor superclass. Sebelum subclass menjalankan kontruktornya sendiri, subclass akan menjalankan kontruktor superclass terlebih dahulu.


## Percobaan

### Percobaan 1 (Extends)

#### Tahapan Percobaan

1.  Buatlah sebuah class parent/superclass dengan nama ClassA.java

    ```java
    public class Percobaan1_1841720105Hafidh {
        public static void main(String[] args) {
            ClassB1841720105Hafidh hitung = new ClassB1841720105Hafidh();
            hitung.x = 20;
            hitung.y = 30;
            hitung.z = 5;
            hitung.getNilaiHafidh();
            hitung.getNilaiZHafidh();
            hitung.getJumlahHafidh();
        }
    }
    ```
2. Buatlah sebuah class anak/subclass dengan nama ClassB.java

    ```java
    public class ClassB1841720105Hafidh {
        public int z;
        public void getNilaiZHafidh(){
            System.out.println("Nilai Z: "+z);
        }
    
        public void getJumlahHafidh(){
            System.out.println("Jumlah: "+(x+y+z));
        }
    }
    ```
   
3. Buatlah class Percobaan1.java untuk menjalankan program diatas!

    ```java
    public class Percobaan1_1841720105Hafidh {
        public static void main(String[] args) {
            ClassB1841720105Hafidh hitung = new ClassB1841720105Hafidh();
            hitung.x = 20;
            hitung.y = 30;
            hitung.z = 5;
            hitung.getNilaiHafidh();
            hitung.getNilaiZHafidh();
            hitung.getJumlahHafidh();
        }
    }
    ```
4. Jalankan program diatas, kemudian amati apa yang terjadi!
![](img/percobaan1.PNG)


Link kode program 

[Class A](../../src/06_Inheritance/praktikum/percobaan1/ClassA1841720105Hafidh.java)

[Class B](../../src/06_Inheritance/praktikum/percobaan1/ClassB1841720105Hafidh.java)

[Main ](../../src/06_Inheritance/praktikum/percobaan1/Percobaan1_1841720105Hafidh.java)

#### Pertanyaan



#### class main Percobaan1

### Pertanyaan
Pertanyaan Berdasarkan percobaan 
jawablah pertanyaan‑pertanyaan yang terkait: 

1. Pada percobaan 1 diatas program yang dijalankan terjadi error, kemudian perbaiki sehingga program tersebut bisa dijalankan dan tidak error!

``jawab :``

```java
public class ClassB1841720105Hafidh extends ClassA1841720105Hafidh {
    public int z;
    public void getNilaiZHafidh(){
        System.out.println("Nilai Z: "+z);
    }

    public void getJumlahHafidh(){
        System.out.println("Jumlah: "+(x+y+z));
    }
}
```

2. Jelaskan apa penyebab program pada percobaan 1 ketika dijalankan terdapat error!

``jawab`` 

penyebabnya adalah belum ada pendeklarasian pewarisan class dari ``CLassA`` sehingga terjadi error

 
### Percobaan 2

#### Tahapan Percobaan

1. Buatlah sebuah class parent/superclass dengan nama ClassA.java
    ```java
    public class ClassA1841720105Hafidh {
        private int x;
        private int y;
    
        public void setXHafidh(int x){
            this.x = x;
        }
        public void setYHafidh(int y){
            this.y = y;
        }
        public void getNilaiHafidh(){
            System.out.println("Nilai x:"+x);
            System.out.println("Nilai y:"+y);
    
        }
    }
    
    ```
2. Buatlah sebuah class anak/subclass dengan nama ClassB.java
    ```java
    public class ClassB1841720105Hafidh extends ClassA1841720105Hafidh {
        public int z;
    
        public void setZHafidh(int z) {
            this.z = z;
        }
    
        public void getNilaiZHafidh(){
            System.out.println("Nilai Z: "+z);
        }
    
        public void getJumlahHafidh(){
            System.out.println("Jumlah: "+(x+y+z));
        }
    }
    ```
3. Buatlah class Percobaan2.java untuk menjalankan program diatas!
    ```java
    public class Percobaan2_1841720105Hafidh {
        public static void main(String[] args) {
            ClassB1841720105Hafidh hitung = new ClassB1841720105Hafidh();
            hitung.setXHafidh(20);
            hitung.setYHafidh(30);
            hitung.setZHafidh(5);
            hitung.getNilaiHafidh();
            hitung.getNilaiZHafidh();
            hitung.getJumlahHafidh();
        }
    }
    ```
4. Jalankan program diatas, kemudian amati apa yang terjadi!
![](img/percobaan2.PNG)


Link kode program 

[Class A](../../src/06_Inheritance/praktikum/percobaan2/ClassA1841720105Hafidh.java)

[Class B](../../src/06_Inheritance/praktikum/percobaan2/ClassB1841720105Hafidh.java)

[Main ](../../src/06_Inheritance/praktikum/percobaan2/Percobaan2_1841720105Hafidh.java)

#### Pertanyaan

1. Pada percobaan 2 diatas program yang dijalankan terjadi error, kemudian perbaiki sehingga program tersebut bisa dijalankan dan tidak error!

    ``jawab :``

    ```java
    public class ClassA1841720105Hafidh {
        protected int x;
        protected int y;
    
        public void setXHafidh(int x){
            this.x = x;
        }
        public void setYHafidh(int y){
            this.y = y;
        }
    
        public void getNilaiHafidh(){
            System.out.println("Nilai x:"+x);
            System.out.println("Nilai y:"+y);
    
        }
    }
    ```
    
    perubahan akses modifier x dan y menjadi protected

2. Jelaskan apa penyebab program pada percobaan 1 ketika dijalankan terdapat error!	

jawab :
 
karena modifier sebelumnya private sehingga variable tidak dapat diakses oleh kelas lain 

### Percobaan 3

#### Tahapan Percobaan
1. Buatlah sebuah class parent/superclass dengan nama Bangun.java
    ```java
    public class Bangun1841720105Hafidh {
        protected double mPhi;
        protected int mR;
    }
    ```
    
2. Buatlah sebuah class anak/subclass dengan nama Tabung.java
    ```java
    public class Tabung1841720105Hafidh extends Bangun1841720105Hafidh {
        protected int t;
        public void setSuperPhiHafidh(double phi){
            super.mPhi = phi;
        }
        public void setSuperRHafidh(int r){
            super.mR = r;
        }
    
        public void setTHafidh(int t) {
            this.t = t;
        }
        public void volumeHafidh(){
            System.out.println("Volume Tabung adalah: "+(super.mPhi *super.mR *super.mR *this.t));
        }
    }
    ```

3. Buatlah class Percobaan3.java untuk menjalankan program diatas!
    ```java
    public class Percobaan3_1841720105Hafidh {
        public static void main(String[] args) {
            Tabung1841720105Hafidh tabung = new Tabung1841720105Hafidh();
            tabung.setSuperPhiHafidh(3.14);
            tabung.setSuperRHafidh(10);
            tabung.setTHafidh(3);
            tabung.volumeHafidh();
        }
    }
    ```

4. Jalankan program diatas!

![](img/percobaan3.PNG)

Link kode program 

[Bangun](../../src/06_Inheritance/praktikum/percobaan3/Bangun1841720105Hafidh.java)

[Tabung](../../src/06_Inheritance/praktikum/percobaan3/Tabung1841720105Hafidh.java)

[Main ](../../src/06_Inheritance/praktikum/percobaan3/Percobaan3_1841720105Hafidh.java)


#### Pertanyaan

1. Jelaskan fungsi “super” pada potongan program berikut di class Tabung!

jawab:

Super digunakan untuk merepresenta-sikan objek dari class induk. 

2. Jelaskan fungsi “super” dan “this” pada potongan program berikut di class Tabung! 

    ![](img/percobaan3_soal2.PNG)

    ``jawab :``

    karena phi dan r berasal dari class induk maka harus direpresentasikan dengan super. 

 
3. Jelaskan mengapa pada class Tabung tidak dideklarasikan atribut “phi” dan “r” tetapi class tersebut dapat mengakses atribut tersebut.

    `jawab :`
    
    attribut phi dan y sudah di deklarasikan di parent dari class Tabung sehingga tidak diperlukan untuk pendeklarasian attribut phi dan r tetapi cukup dengan mengaksesnya dari class parent tersebut

### Percobaan 4

#### Tahapan Percobaan

1.  Buatlah tiga file dengan nama ClassA.java , ClassB.java , dan ClassC.java, seperti pada kode program dibawah ini!
    ClassA.java
    ```java
    public class ClassA1841720105Hafidh {
        public ClassA1841720105Hafidh() {
            System.out.println("Konstruktor A dijalankan");
        }
    }
    ```
    ClassB.java
    ```java
    public class ClassB1841720105Hafidh extends ClassA1841720105Hafidh {
        ClassB1841720105Hafidh() {
            System.out.println("Konstruktor B dijalankan");
        }
    }
    ```
    ClassC.java
    ```java
    public class ClassC1841720105Hafidh extends ClassB1841720105Hafidh {
        ClassC1841720105Hafidh() {
            super();
            System.out.println("Konstruktor C dijalankan");
        }
    }
    ```
2. Buatlah class Percobaan4.java untuk menjalankan program diatas!
    ```java
    public class Percobaan4_1841720105Hafidh {
        public static void main(String[] args) {
            ClassC1841720105Hafidh test = new ClassC1841720105Hafidh();
        }
    }
    ```

3. Jalankan program kemudian amati apa yang terjadi!
    ![](img/percobaan4.PNG)

Link kode program 

[Class A](../../src/06_Inheritance/praktikum/percobaan3/Bangun1841720105Hafidh.java)

[Class B](../../src/06_Inheritance/praktikum/percobaan3/Tabung1841720105Hafidh.java)

[Main ](../../src/06_Inheritance/praktikum/percobaan3/Percobaan3_1841720105Hafidh.java)

#### Pertanyaan

1. Pada percobaan 4 sebutkan mana class yang termasuk superclass dan subclass, kemudian jelaskan alasannya!

    `jawab :`

    Yang merupakan super class yaitu ClassA. ClassB dan ClassC termasuk subclass karena ClassB dan ClassC mewarisi ClassA 

2. Ubahlah isi konstruktor default ClassC seperti berikut:	 Tambahkan kata super() di baris	 Pertaman dalam konstruktor defaultnya. Coba jalankan kembali class Percobaan4 dan terlihat tidak ada perbedaan dari hasil outputnya!	

    `jawab :`
    
    ```java
        public class ClassC1841720105Hafidh extends ClassB1841720105Hafidh {
            ClassC1841720105Hafidh() {
                super();
                System.out.println("Konstruktor C dijalankan");
            }
        }
    ```
 
3. Ublah isi konstruktor default ClassC seperti berikut:
    ```java
    public class ClassC1841720105Hafidh extends ClassB1841720105Hafidh {
        ClassC1841720105Hafidh() {
            System.out.println("Konstruktor C dijalankan");
            super();
        }
    }
    ```
   Ketika mengubah posisi super() dibaris kedua dalam kontruktor defaultnya dan terlihat ada error. Kemudian kembalikan super() kebaris pertama seperti sebelumnya, maka errornya akan hilang. Perhatikan hasil keluaran ketika class Percobaan4 dijalankan. Kenapa bisa tampil output seperti berikut pada saat instansiasi objek test dari class ClassC :
   Jelaskan bagaimana urutan proses jalannya konstruktor saat objek test dibuat!

    `jawab :`

    Pertama program membaca konstruktor ClassA, kemudian lanjut ke konstruktor ClassB yang di extends dengan ClassA, selanjutnya lanjut ke ClassC yang di extends ClassB, maka ketiga class tersebut dapat terhubung secara urut

4. Apakah fungsi super() pada potongan program dibawah ini di ClassC!

    `jawab :`

    Untuk memanggil konstruktor parent class, karena tidak ada yang dirujuk maka output tidak berpengaruh 

## Percobaan 5

#### Tahapan Percobaan

Perhatikan diagram class dibawah ini:

[](img/percobaan5_uml.PNG)

1. Buatlah class Karyawan
    ```java
    public class Karyawan1841720105Hafidh {
        public String nama, alamat, jk;
        public int umur, gaji;
    
        public Karyawan1841720105Hafidh() {
        }
    
        public Karyawan1841720105Hafidh(String nama, String alamat, String jk, int umur, int gaji) {
            this.nama = nama;
            this.alamat = alamat;
            this.jk = jk;
            this.umur = umur;
            this.gaji = gaji;
        }
        public void tampilDataKaryawanHafidh(){
            System.out.println("Nama                = "+nama);
            System.out.println("Alamt               = "+alamat);
            System.out.println("Jenis Kelamin       = "+jk);
            System.out.println("Umur                = "+umur);
            System.out.println("Gaji                = "+gaji);
    
        }
    }
    ```
2. Buatlah class Manager
    ```java
    public class Manager1841720105Hafidh extends Karyawan1841720105Hafidh {
        public int tunjangan;
    
        public Manager1841720105Hafidh(){
    
        }
    
        public void tampilDataManagerHafidh(){
            super.tampilDataKaryawanHafidh();
            System.out.println("Tunjangan           = "+tunjangan);
            System.out.println("Total Gaji          = "+(super.gaji+tunjangan));
        }
    }
    ```
3. Buatlah class Staff
    ```java
    public class Staff1841720105Hafidh extends Karyawan1841720105Hafidh {
        public int lembur, potongan;
    
        public Staff1841720105Hafidh() {
        }
    
        public Staff1841720105Hafidh(String nama, String alamat, String jk, int umur, int gaji, int lembur, int potongan) {
            super(nama, alamat, jk, umur, gaji);
            this.lembur = lembur;
            this.potongan = potongan;
        }
        public void tampilDataStaffHafidh(){
            super.tampilDataKaryawanHafidh();
            System.out.println("Lembur              = "+lembur);
            System.out.println("Potongan            = "+potongan);
            System.out.println("Total Gaji          = "+(gaji+lembur+potongan));
        }
    }
    ```
4. Buatlah class inheritance1
    ```java
    public class Inheritance1_1841720105Hafidh {
        public static void main(String[] args) {
            Manager1841720105Hafidh M = new Manager1841720105Hafidh();
            M.nama = "Vivin";
            M.alamat = "Jl. Vinolia";
            M.umur = 25;
            M.jk = "Perempuan";
            M.gaji = 3000000;
            M.tunjangan =  1000000;
            M.tampilDataManagerHafidh();
    
            Staff1841720105Hafidh S = new Staff1841720105Hafidh();
            S.nama = "Lestari";
            S.alamat = "Malang";
            S.umur = 25;
            S.jk = "Perempuan";
            S.gaji = 2000000;
            S.lembur = 500000;
            S.potongan = 250000;
            S.tampilDataStaffHafidh();
    
        }
    }
    ```
5. Malankan program, maka tampilanya adalah sebagai berikut:
![](img/percobaan5.PNG)

Link kode program 

[Karyawan](../../src/06_Inheritance/praktikum/percobaan5dan6/Karyawan1841720105Hafidh.java)

[Manager](../../src/06_Inheritance/praktikum/percobaan5dan6/Manager1841720105Hafidh.java)

[Staff](../../src/06_Inheritance/praktikum/percobaan5dan6/Staff1841720105Hafidh.java)

[Main ](../../src/06_Inheritance/praktikum/percobaan5dan6/Inheritance1_1841720105Hafidh.java )


#### Pertanyaan


1. Sebutkan class mana yang termasuk super class dan sub class dari percobaan 1 diatas!
   
    `jawab :`

    Class Kaaryawan merupakan superclass, class Manager dan Staff merupaka subclass 

2. Kata kunci apakah yang digunakan untuk menurunkan suatu class ke class yang lain?

    `jawab :`

    Extends 

3. Perhatikan kode program pada class Manager, atribut apa saja yang dimiliki oleh class tersebut? Sebutkan atribut mana saja yang diwarisi dari class Karyawan!

    `jawab :`

    Class Manager hanya memiliki atribut tunjangan, sedangkan yang diwarisi oleh class Manager yaitu : nama, alamat, jk, umur dan gaji

4. Jelaskan kata kunci super pada potongan program dibawah ini yang terdapat pada class Manager!

    `jawab :`

    Untuk memanggil atribut gaji di class Karyawan 

5. Program pada percobaan 1 diatas termasuk dalam jenis inheritance apa? Jelaskan alasannya!
    
    `jawab :`
    
    Single Inheritance karena subclass hanya memiliki 1 parents 

### Percobaan 6
#### Tahapan Percobaan
1.  Perhatikan digram class dibawah ini
![](img/percobaan6_uml.PNG)

2. Berdasarkan program yang sudah anda buat pada percobaan 1 sebelumnya tambahkan dua class yaitu class StaffTetap dan class StaffHarian. Kode Programnya adalah sebagai berikut
Class StaffTetap.java
    ```java
    public class StaffTetap1841720105Hafidh extends Staff1841720105Hafidh {
        public String golongan;
        public int asuransi;
    
        public StaffTetap1841720105Hafidh(){
    
        }
    
        public StaffTetap1841720105Hafidh(String nama, String alamat, String jk, int umur, int gaji, int lembur, int potongan, String golongan, int asuransi) {
            super(nama, alamat, jk, umur, gaji, lembur, potongan);
            this.golongan = golongan;
            this.asuransi = asuransi;
        }
        public void tampilStaffTetapHafidh(){
            System.out.println("========Data Staff Tetap==========");
            super.tampilDataStaffHafidh();
            System.out.println("Golongan            = "+golongan);
            System.out.println("Jumlah Asuransi     = "+asuransi);
            System.out.println("Gaji Bersih         = "+(gaji+lembur-potongan-asuransi));
        }
    }
    ```
    Class StaffHarian.java
    ```java
    public class StaffHarian1841720105Hafidh extends Staff1841720105Hafidh {
        public int jmlJamKerja;
        public StaffHarian1841720105Hafidh(){
    
        }
    
        public StaffHarian1841720105Hafidh(String nama, String alamat, String jk, int umur, int gaji, int lembur, int potongan, int jmlJamKerja) {
            super(nama, alamat, jk, umur, gaji, lembur, potongan);
            this.jmlJamKerja = jmlJamKerja;
        }
        public void tampilStaffHarianHafidh(){
            System.out.println("========Data Staff Harian==========");
            super.tampilDataStaffHafidh();
            System.out.println("Jumlah Jam Kerja    = "+jmlJamKerja);
            System.out.println("Gaji Bersih         = "+(gaji*jmlJamKerja+lembur-potongan));
        }
    }
    ```

3. Setelah membuat dua class diatas kemudian edit class inheritance1.java menjadi sebagai berikut:
    ```java
    public class Inheritance1_1841720105Hafidh {
        public static void main(String[] args) {
    StaffTetap1841720105Hafidh ST = new StaffTetap1841720105Hafidh("Budi", "Malang", "Laki-Laki", 20, 2000000, 250000, 200000, "2A", 100000);
            ST.tampilStaffTetapHafidh();
    
            StaffHarian1841720105Hafidh SH = new StaffHarian1841720105Hafidh("Indah", "Malang", "Perempuan", 27, 10000, 100000, 50000, 100);
            SH.tampilStaffHarianHafidh();
        }
    }
    ``` 

4. Jalankan program maka tampilanny adalah sebagai berikut:

![](img/percobaan6.PNG)

Link kode program 

[StaffHarian](../../src/06_Inheritance/praktikum/percobaan5dan6/StaffHarian1841720105Hafidh.java)

[StaffTetap](../../src/06_Inheritance/praktikum/percobaan5dan6/StaffTetap1841720105Hafidh.java)

[Main ](../../src/06_Inheritance/praktikum/percobaan5dan6/Inheritance1_1841720105Hafidh.java )

#### Pertanyaan

1. Berdasarkan class diatas manakah yang termasuk single inheritance dan mana yang termasuk multilevel inheritance?

    ``jawab :``

    Single inheritance Staff dan Manager dan yang multilevel inheritance Staff Tetap dan Stdaffharian 

2.	 Perhatikan kode program class StaffTetap dan StaffHarian, atribut apa saja yang dimiliki oleh class tersebut? Sebutkan atribut mana saja yang diwarisi dari class Staff!

    ``jawab :``

    Staff tetap golongan dan asuransi, staff harian jumlah jam kerja 

3.	Apakah fungsi potongan program berikut pada class StaffHarian?

    ``jawab :``

    Untuk memanggil atribut tersebut dari class  induk 

4. Apakah fungsi potongan program berikut pada class StaffHarian?

    ``jawab :``

    Untuk memanggil method tersebut dari class  induk 

5.	 Perhatikan kode program dibawah ini yang terdapat pada class StaffTetap
    Terlihat dipotongan program diatas atribut gaji, lembur dan potongan dapat diakses langsung. Kenapa hal ini bisa terjadi dan bagaimana class StaffTetap memiliki atribu gaji, lembur, dan potongan padahal dalam class tersebut tidak dideklarasikan atribut gaji, lembur, dan potongan?

    ``jawab :``

    Hal itu dapat terjadi karena sudah ditulis dalam parameter konstruktor class tersebut 

## Tugas

![](img/tugas_uml.PNG)

![](img/tugas.PNG)

link kode program 

[Komputer](../../src/06_Inheritance/tugas/Komputer1841720105Hafidh.java)

[Laptop](../../src/06_Inheritance/tugas/Laptop1841720105Hafidh.java)

[Pc](../../src/06_Inheritance/tugas/Pc1841720105Hafidh.java)

[Mac](../../src/06_Inheritance/tugas/Mac1841720105Hafidh.java)

[Windows](../../src/06_Inheritance/tugas/Windows1841720105Hafidh.java)

[Main](../../src/06_Inheritance/tugas/Main1841720105Hafidh.java)


## Kesimpulan

Dalam percobaan ini terdapat dua istilah yang sering digunakan. Kelas yang menurunkan disebut kelas dasar (base class/super class), sedangkan kelas yang diturunkan disebut kelas turunan (derived class/sub class/child class) . Di dalam Java untuk mendeklarasikan suatu class sebagai subclass dilakukan dengan cara menambahkan kata kunci extends setelah deklarasi nama class,kemudian diikuti dengan nama parent class-nya.

## Pernyataan Diri

Saya menyatakan isi tugas, kode program, dan laporan praktikum ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.

Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.

Ttd,


***(Hafidh Sajid M)*** 
