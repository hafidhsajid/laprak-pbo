# Laporan Praktikum #1 - Pengantar Konsep PBO

## Kompetensi

Setelah menempuh materi percobaan ini, mahasiswa mampu mengenal:

1. Perbedaan paradigma berorientasi objek dengan paradigma struktural
2. Konsep dasar PBO

## Ringkasan Materi

(berisi catatan penting pribadi selama praktikum berlangsung ataupun menemukan permasalahan khusus saat melakukan percobaan)

## Percobaan

### Percobaan 1


Didalam percobaan ini, kita akan mendemonstrasikan bagaimana membuat class, membuat object, 
kemudian mengakses method didalam class tersebut.

1. Buka Netbeans, buat project SepedaDemo.
2. Buat class Sepeda. Klik kanan pada package sepedademo – New – Java Class.
3. Ketikkan kode class Sepeda dibawah ini.
![satu](img/satu.png)
4. Kemudian pada class main, ketikkan kode berikut ini.
![dua main](img/duamain.png)
5. Cocokkan hasilnya! <br>
![tiga hasil](img/tigahasil.png)

link program : <br>
[ini link ke main kode program](../../src/01_Pengantar_Konsep_PBO/sepedademo/Sepedamain.java) <br>
[ini link ke source program](../../src/01_Pengantar_Konsep_PBO/sepedademo/Sepeda.java)

### Percobaan 2

Didalam percobaan ini, akan didemonstrasikan salah satu fitur yang paling penting dalam PBO, yaitu inheritance. Disini kita akan membuat class SepedaGunung yang mana adalah turunan/warisan dari class Sepeda. Pada dasarnya class SepedaGunung adalah sama dengan class Sepeda, hanya saja pada sepeda gunung terdapat tipe suspensi. Untuk itu kita tidak perlu membuat class Sepeda Gunung dari nol, tapi kita wariskan saja class Sepeda ke class SepedaGunungPenjelasan lebih detail tentang inheritance akan dibahas pada modul selanjutnya.

1. Masih pada project SepedaDemo. Buat class SepedaGunung.
2. Tambahkan kode extends Sepeda pada deklarasi class SepedaGunung. Kode extends ini menandakan bahwa class SepedaGunung mewarisi class Seped
![empat](img/empat.png)
3. Kemudian lengkapi kode SepedaGunung seperti berikut ini:
![lima](img/lima.png)
4. Kemudian pada class main, tambahkan kode berikut ini:
![enam](img/enam.png)
5. Cocokkan hasilnya <br>
![tujuh](img/tujuh.png)


link program : <br>
[link ke main program](../../src/01_Pengantar_Konsep_PBO/sepedademo/Sepedamain.java) <br>
[ini link ke program source ](../../src/01_Pengantar_Konsep_PBO/sepedademo/Sepeda.java) <br>
[ini link ke source program Sepeda Gunung ](../../src/01_Pengantar_Konsep_PBO/sepedademo/Sepeda.java)

## Pertanyaan

1. Sebutkan dan jelaskan aspek-aspek yang ada pada pemrograman berorientasi objek!
2. Apa yang dimaksud dengan object dan apa bedanya dengan class?
3. Sebutkan salah satu kelebihan utama dari pemrograman berorientasi objek dibandingkan dengan pemrograman struktural!
4. Pada class Sepeda, terdapat state/atribut apa saja?
5. Tambahkan atribut warna pada class Sepeda.
6. Mengapa pada saat kita membuat class SepedaGunung, kita tidak perlu membuat class nya dari nol?

## Jawab

1. - Abstraction merupakan salah satu cara pengabstrakan atau penyembunyian kerumitan suatu proses. Sehingga suatu objek dalam bentuk yang lebih sederhana.
    - Encapsulation merupakan suatu mekanisme untuk menyembunyikan atau memproteksi suatu proses.
    - Inheritance merupakan konsep pewarisan attribute dan method yang dimiliki oleh sebuah class kepada turunannya.
    - Polymorphic dapat berarti banyak bentuk, maksudnya yaitu kita dapat menimpa (override), suatu method, yang berasal dari parent class (super class) dimana object tersebut diturunkan, sehingga memiliki kelakuan yang berbeda.

2. Class adalah kumpulan dari suatu fungsi fungsi sedangkan objek adalah pemanfaatan dari class yang siap digunakan.

3. Kelabihan dari PBO adalah untuk meningkatkan produktivitas karena di dalam PBO yang telah dibuat untuk suatu problem masih bisa digunakan untuk problem yang lain

4. rem, merek, warna, kecepatan, gear, seMerek, gantiGear, tamhaKecepatan, cetakStatus

5. <br>![warna](img/pertanyaan5.png)
6. Dikarenakan class SepedaGunung merupakan penurunan dari class Sepeda.

## Tugas

![tugas](img/tugas1source.png)
![tugas](img/tugas1source2.png)
![tugas](img/tugas1main.png)
![tugas](img/tugas1hasil.png)
[link ke program Openwar](../../src/01_Pengantar_Konsep_PBO/Openwar/Tugas.java)

## Kesimpulan

Kesimpulan dari praktikum ini adalah dijelaskan tentang apa itu OOP <br>

## Pernyataan Diri

Saya menyatakan isi tugas, kode program, dan laporan praktikum ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.

Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.

Ttd,

***Hafidh Sajid Malik***
