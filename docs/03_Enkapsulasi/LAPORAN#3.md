# Laporan Praktikum #3 - Enkapsulasi

## Kompetensi

Setelah melakukan percobaan pada modul ini, mahasiswa memahami konsep:

1. Konstruktor
2. Akses Modifier
3. Atribut/method pada class
4. Intansiasi atribut/method
5. Setter dan getter
6. Memahami notasi pada UML Class Diagram

## Ringkasan Materi

Pada pertemuan pertama dan kedua telah dibahasan konsep dasar dari pemrograman berbasis
objek (PBO), perbedaan antara pemrograman berbasis objek dengan pemrograman struktural, dan
telah dibahas konsep class dan object pada PBO. Selanjutnya pada modul ini akan dibahas konsep
enkapsulasi pada PBO dan notasi yang ada pada UML Class diagram.

### Enkapsulasi
Pada modul pertama telah dijabarkan definisi dari enkapsulasi sebagai berikut:
Enkapsukasi disebut juga dengan information-hiding. Dalam berinteraksi dengan objek, seringkali
kita tidak perlu mengetahui kompleksitas yang ada didalamnya. Hal ini akan lebih mudah dipahami
jika kita membayangkan atau menganalisa objek yang ada disekitar kita, misalnya objek sepeda,
ketika kita mengganti gear pada sepeda, kita tinggal menekan tuas gear yang ada di grip setang
sepeda saja. Kita tidak perlu mengetahui bagaimana cara gear berpindah secara teknis. Contoh objek
lain misalnya mesin penghisap debu (vacum cleaner), ketika kita mencolokkan kabel vacum cleaner
dan menyalakan sakelarnya maka mesin tersebut siap digunakan untuk menghisap debu. Dalam
proses tersebut kita tidak mengetahui proses rumit yang terjadi ketika mengubah listrik menjadi
tenaga dari vacum cleaner. Dalam contoh diatas vacum cleaner dan sepeda telah menerapkan
enkapsulasi atau disebut juga information-hiding atau data hiding karena menyembunyikan detail
proses suatu objek dari pengguna.

### Konstruktor

Konstruktor mirip dengan method cara deklarasinya akan tetapi tidak memiliki tipe return. Dan
konsturktor dieksekusi ketika instan dari objek dibuat. Jadi setiap kali sebuat objek dibuat dengan
keyword new() maka konstruktor akan dieksekusi. Cara untuk membuat konstruktor adalah sebagai
berikut:
    1. Nama konstruktor harus sama dengan nama class
    2. Konstruktor tidak memiliki tipe data return
    3. Konstruktor tidak boleh menggunakan modifier abstract, static, final, dan syncronized
    
Note: Di java kita dapat memiliki konstruktor dengan modifier private, protected, public or default.

### Akses Modifier

Terdapat 2 tipe modifier di java yaitu : akses modifier dan non-access modifier. Dalam hal ini kita
akan fokus pada akses modifier yang berguna untuk mengatur akses method, class, dan constructor.
Terdapat 4 akses modifier yaitu:
1. private – hanya dapat diakses di dalam kelas yang sama
2. default – hanya dapat diakses di dalam package yang sama
3. protected – dapat diakases di luar package menggunakan subclass (membuat inheritance)
4. public – dapat diakases dari mana saja

### Getter dan Setter
Getter adalah public method dan memiliki tipe data return, yang berfungsi untuk mendapatkan nilai
dari atribut private. Sedangkan setter adalah public method yang tidak memliki tipe data return,
yang berfungsi untuk memanipulasi nilai dari atribut private.

## Percobaan

### Percobaan 1: Enkapsulasi

Didalam percobaan enkapsulasi, buatlah class Motor yang memiliki atribut kecepatan dan kontakOn,
dan memiliki method printStatus() untuk menampilkan status motor. Seperti berikut

1. Buka Netbeans, buat project MotorEncapsulation.
2. Buat class Motor. Klik kanan pada package motorencapsulation – New – Java Class.
3. Ketikkan kode class Motor dibawah ini <br>
![1](img/percobaan1.png) <br>
bentuk UML class diagram class Motor adalah sebagai berikut:<br>
![2](img/percobaan1.1.png)
4. Kemudian buat class MotorDemo, ketikkan kode berikut ini. <br>
![3](img/percobaan1_2.png)
5. Hasilnya adalah sebagai berikut:
![3](img/percobaan1hasil.png)

Dari percobaan 1 - enkapsulasi, menurut anda, adakah yang janggal?
Yaitu, kecepatan motor tiba-tiba saja berubah dari 0 ke 50. Lebih janggal lagi, posisi kontak motor
masih dalam kondisi OFF. Bagaimana mungkin sebuah motor bisa sekejap berkecepatan dari nol ke
50, dan itupun kunci kontaknya OFF?
Nah dalam hal ini, akses ke atribut motor ternyata tidak terkontrol. Padahal, objek di dunia nyata
selalu memiliki batasan dan mekanisme bagaimana objek tersebut dapat digunakan. Lalu,
bagaimana kita bisa memperbaiki class Motor diatas agar dapat digunakan dengan baik? Kita bisa
pertimbangkan beberapa hal berikut ini:

1. Menyembunyikan atribut internal (kecepatan, kontakOn) dari pengguna (class lain)
2. Menyediakan method khusus untuk mengakses atribut.
Untuk itu mari kita lanjutkan percobaan berikutknya tentang Access Modifier.

    link program : <br>
    
   [Source program](../../src/03_Enkapsulasi/percobaan/Motor1841720105Hafidh.java) <br>
   [Main program](../../src/03_Enkapsulasi/percobaan/MotorDemo1841720105Hafidh.java) <br>
### Percobaan 2: Membuat dan mengakses anggota suatu class

Pada percobaan ini akan digunakan access modifier untuk memperbaiki cara kerja class Motor pada
percobaan ke-1.

1. Ubah cara kerja class motor sesuai dengan UML class diagram berikut.<br>
![1](img/percobaan2_1.png)
2. Berdasarkan UML class diagram tersebut maka class Motor terdapat perubahan, yaitu:

    - Ubah access modifier kecepatan dan kontakOn menjadi private
    - Tambahkan method nyalakanMesin, matikanMesin tambahKecepatan, kurangiKecepatan. <br> <br>

    Implementasi class Motor adalah sebagai berikut: <br>
![2](img/percobaan2_2.png)

3. Kemudian pada class MotorDemo, ubah code menjadi seperti berikut:
![2](img/percobaan2_3.png)

4. Hasilnya dari class MotorDemo adalah sebagai berikut:
![2](img/percobaan2hasil.png)

    Dari percobaan diatas, dapat kita amati sekarang atribut kecepatan tidak bisa diakses oleh pengguna dan diganti nilainya secara sembarangan. Bahkan ketika mencoba menambah kecepatan saat posisi kontak masih OFF, maka akan muncul notifikasi bahwa mesin OFF. Untuk mendapatkan kecepatanyang diinginkan, maka harus dilakukan secara gradual, yaitu dengan memanggil method tambahKecepatan() beberapa kali. Hal ini mirip seperti saat kita mengendarai motor.

    link program : <br>
    
   [Source program](../../src/03_Enkapsulasi/percobaan/Motor1841720105Hafidh.java) <br>
   [Main program](../../src/03_Enkapsulasi/percobaan/MotorDemo1841720105Hafidh.java) <br>
### Pertanyaan

1. Pada class TestMobil, saat kita menambah kecepatan untuk pertama kalinya, mengapa muncul peringatan “Kecepatan tidak bisa bertambah karena Mesin Off!”?
2. Mengapat atribut kecepatan dan kontakOn diset private?
3. Ubah class Motor sehingga kecepatan maksimalnya adalah 100!

Jawab:

1. Karena pada method `tambahKecepatan()` terdapat perintah yang berisi ketika variabel kontak on memiliki nilai false maka tidak dapat ditambah maka kecepatan tidak akan bertambah tetapi mengeluarkan pesan bahwa "Kecepatan tidak bisa bertambah"
2. Agar atribut kecepatan dan kontakOn menjadi tersembunyi dan tidak dapat digunakan set secara langsung dari luar class.
3. 
![](img/pertanyaan1no3.png)

### Percobaan 3:  Getter dan Setter

Misalkan di sebuah sistem informasi koperasi, terdapat class Anggota. Anggota memiliki atribut
nama, alamat dan simpanan, dan method setter, getter dan setor dan pinjam. Semua atribut pada
anggota tidak boleh diubah sembarangan, melainkan hanya dapat diubah melalui method setter,
getter, setor dan tarik. Khusus untuk atribut simpanan tidak terdapat setter karena simpanan akan
bertambah ketika melakukan transaksi setor dan akan berkurang ketika melakukan peminjaman/tarik

1. Berikut ini UML class buatlah class Mahasiswa pada program: <br>
![](img/percobaan3_1.png)
2. Sama dengan percobaan 1 untuk membuat project baru
   - Buka Netbeans, buat project KoperasiGetterSetter.
   - Buat class Anggota. Klik kanan pada package koperasigettersetter – New – Java
   Class.
   - Ketikkan kode class Anggota dibawah ini. <br>
   ![](img/percobaan3_2.png)<br>
   Jika diperhatikan pada class Anggota, atribut nama dan alamat memili masing-masing 1
   getter dan setter. Sedangkan atribut simpanan hanya memiliki getSimpanan() saja, karena
   seperti tujuan awal, atribut simpanan akan berubah nilainya jika melakukan transaksi setor()
   dan pinjam/tarik().
  3. Selanjutnya buatlah class KoperasiDemo untuk mencoba class Anggota.
   ![](img/percobaan3_3.png)
   
  4. Hasil dari main method pada langkah ketiga adalah 
   ![](img/percobaan3hasil.png)
   Dapat dilihat pada hasil percobaan diatas, untuk mengubah simpanan tidak dilakukan secara
   langsung dengan mengubah atribut simpanan, melainkan melalui method setor() dan pinjam().
   Untuk menampilkan nama pun harus melalui method getNama(), dan untuk menampilkan simpanan
   melalui getSimpanan().<br>
   
   link program : <br>
    
   [Source program](../../src/03_Enkapsulasi/koperasigettersetter/Anggota1841720105Hafidh.java) <br>
   [Main program](../../src/03_Enkapsulasi/koperasigettersetter/KoperasiDemo1841720105Hafidh.java) <br>
### Percobaan 4:  Konstruktor, Instansiasi

1. Langkah pertama percobaan 4 adalah ubah class KoperasiDemo seperti berikut<br>
   ![](img/percobaan4_1.png)
2. Hasil dari program tersebut adalah sebagai berikut<br>
   ![](img/percobaan4hasil1.png)
   Dapat dilihat hasil running program, ketika dilakukan pemanggilan method getNama()
   hasilnya hal ini terjadi karena atribut nama belum diset nilai defaultnya. Hal ini dapat
   ditangani dengan membuat kontruktor.
3. Ubah class Anggota menjadi seperti berikut<br>
   ![](img/percobaan4_3.png) <br>
   Pada class Anggota dibuat kontruktor dengan access modifier default yang memiliki 2
   parameter nama dan alamat. Dan didalam konstruktor tersebut dipastikan nilai simpanan
   untuk pertama kali adalah Rp. 0.
4. Selanjutnya ubah class KoperasiDemo sebagai berikut<br>
   ![](img/percobaan4_4.png)
5. Hasil dari program tersebut adalah sebagai berikut<br>
   ![](img/percobaan4hasil2.png) <br>
   Setelah menambah konstruktor pada class Anggoata maka atribut nama dan alamat secara
   otomatis harus diset terlebih dahulu dengan melakukan passing parameter jika melakukan
   instansiasi class Anggota. Hal ini biasa dilakukan untuk atribut yang membutuhkan nilai yang
   spesifik. Jika tidak membutuhkan nilai spesifik dalam konstruktor tidak perlu parameter.
   Contohnya simpanan untuk anggota baru diset 0, maka simpanan tidak perlu untuk dijadikan
   parameter pada konstruktor.
   
    link program : <br>
    
   [Source program](../../src/03_Enkapsulasi/koperasigettersetter/Anggota1841720105Hafidh.java) <br>
   [Main program](../../src/03_Enkapsulasi/koperasigettersetter/KoperasiDemo1841720105Hafidh.java) <br>
### Pertanyaan - Percobaan 3 dan 4

1. Apa yang dimaksud getter dan setter?
2. Apa kegunaan dari method getSimpanan()?
3. Method apa yang digunakan untk menambah saldo?
4. Apa yand dimaksud konstruktor?
5. Sebutkan aturan dalam membuat konstruktor?
6. Apakah boleh konstruktor bertipe private?
7. Kapan menggunakan parameter dengan passsing parameter?
8. Apa perbedaan atribut class dan instansiasi atribut?
9. Apa perbedaan class method dan instansiasi method?

#### Jawab

1. Getter adalah public method dan memiliki tipe data return, yang berfungsi untuk mendapatkan nilai dari atribut private. Sedangkan setter adalah public method yang tidak memliki tipe data return, yang berfungsi untuk memanipulasi nilai dari atribut private.
2. metod tersebut berfungsi sebagai untuk mendapatkan nilai dari variabel mSimpanan
3. method yang digunakan untuk menambah saldo adalah method setor()
4. Konstruktor adalah method yang pertama kali dijalankan pada saat sebuah objek pertama kali diciptakan
5. 
    - Nama konstruktor harus sama dengan nama class
    - Konstruktor tidak memiliki tipe data return
    - Konstruktor tidak boleh menggunakan modifier abstract, static, final, dan syncronized
6. Tidak boleh
7. Pada saat instansiasi
8. Instansiasi class adalah proses perubahan class menjadi objek sedangkan atribut objek dari class
9. instansiasi method adalah proses perubahan method menjadi objek sedangkan class method adalah isi dari method yang ada di dalam class

### Kesimpulan
Dari percobaan diatas, telah dipelajari kosep dari enkapsulasi, kontruktor, access modifier yang
terdiri dari 4 jenis yaitu public, protected, default dan private. Konsep atribut atau method class
yang ada di dalam blok code class dan konsep instansiasi atribut atau method. Cara penggunaan
getter dan setter beserta fungsi dari getter dan setter. Dan juga telah dipelajari atau memahami
notasi UML

### Tugas
1. Cobalah program di bawah ini dan tuliskan hasil outputnya <br>
    ![1](img/tugas1_1.png) <br>
    ![](img/tugas1_2.png)   <br>

    link program : <br>
    
    [Source program](../../src/03_Enkapsulasi/tugas/EncapDemo1841720105Hafidh.java) <br>
    [Main program](../../src/03_Enkapsulasi/tugas/EncapTest1841720105Hafidh.java) <br>

2. Pada program diatas, pada class EncapTest kita mengeset age dengan nilai 35, namun pada
   saat ditampilkan ke layar nilainya 30, jelaskan mengapa.
   
   Jawab: 
   Pada program diatas kita memberikan setting age dengan nilai 35 kemudian berubah menjadi 30 hal ini dikarenakan pada 
   program di atas terdapat perintah`if(newAge>30){age = 30}` yang memiliki makna ketika nilai lebih dari 30 akan di rubah menjadi 30.
   
3.  Ubah program diatas agar atribut age dapat diberi nilai maksimal 30 dan minimal 18.<br>
    ![](img/tugas3.png) 

    link program : <br>
    
    [Source program](../../src/03_Enkapsulasi/tugas/EncapDemo1841720105Hafidh.java) <br>
    [Main program](../../src/03_Enkapsulasi/tugas/EncapTest1841720105Hafidh.java) <br>
   
4. Pada sebuah sistem informasi koperasi simpan pinjam, terdapat class Anggota yang memiliki
   atribut antara lain nomor KTP, nama, limit peminjaman, dan jumlah pinjaman. Anggota
   dapat meminjam uang dengan batas limit peminjaman yang ditentukan. Anggota juga dapat
   mengangsur pinjaman. Ketika Anggota tersebut mengangsur pinjaman, maka jumlah
   pinjaman akan berkurang sesuai dengan nominal yang diangsur. Buatlah class Anggota
   tersebut, berikan atribut, method dan konstruktor sesuai dengan kebutuhan. Uji dengan
   TestKoperasi berikut ini untuk memeriksa apakah class Anggota yang anda buat telah sesuai
   dengan yang diharapkan. <br>
   ![](img/tugas4_1.png) <br>
   ![](img/tugas4.png)
   
    link program : <br>
    
    [Source program](../../src/03_Enkapsulasi/tugas/Anggota1841720105Hafidh.java) <br>
    [Main program](../../src/03_Enkapsulasi/tugas/TestKoperasi1841720105Hafidh.java) <br>

5. Modifikasi soal no. 4 agar nominal yang dapat diangsur minimal adalah 10% dari jumlah
   pinjaman saat ini. Jika mengangsur kurang dari itu, maka muncul peringatan “Maaf,
   angsuran harus 10% dari jumlah pinjaman”.<br>
   ![](img/tugas5.png)<br>
   
    link program : <br>
    
   [Source program](../../src/03_Enkapsulasi/tugas/Anggota1841720105Hafidh.java) <br>
   [Main program](../../src/03_Enkapsulasi/tugas/TestKoperasi1841720105Hafidh.java) <br>

6. Modifikasi class TestKoperasi, agar jumlah pinjaman dan angsuran dapat menerima input
   dari console. <br>
   ![](img/tugas6.png)<br>
   
    link program : <br>
    
   [Source program](../../src/03_Enkapsulasi/tugas/Anggota1841720105Hafidh.java) <br>
   [Main program](../../src/03_Enkapsulasi/tugas/TestKoperasi1841720105Hafidh.java) <br>
   
## Kesimpulan

Dari percobaan diatas, telah dipelajari kosep dari enkapsulasi, kontruktor, access modifier yang
terdiri dari 4 jenis yaitu public, protected, default dan private. Konsep atribut atau method class
yang ada di dalam blok code class dan konsep instansiasi atribut atau method. Cara penggunaan
getter dan setter beserta fungsi dari getter dan setter. Dan juga telah dipelajari atau memahami
notasi UML

## Pernyataan Diri

Saya menyatakan isi tugas, kode program, dan laporan praktikum ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.

Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.

Ttd,

***Hafidh Sajid Malik***
