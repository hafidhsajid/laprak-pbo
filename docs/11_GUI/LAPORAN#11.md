# Laporan Praktikum #11 - GUI

## Kompetensi

1. Membuat aplikasi Graphical User Interface sederhana dengan bahasa pemrograman java;
2. Mengenal komponen GUI seperti frame, label, textfield, combobox, radiobutton, checkbox, 
textarea, menu, serta table;
3. dan Menambahkan event handling pada aplikasi GUI.

## Ringkasan Materi

***GUI (Graphical User Interface)*** 

Pada bab-bab sebelumnya interaksi antara user dengan program hanya berbasis console editor dengan tampilan dos yang membosankan, maka agar interaksi antara user dengan program tidak membosankan diperlukanlah sebuah interface yang menghubungkan antara user dengan program dengan tampilan grafis, interface ini dinamakan dengan GUI (*Graphical User Interface*). Dalam pemrograman GUI terdapat beberapa bagian yang harus dilakukan yaitu:

1. Membuat windows utama.
2. Menentukan komponen-komponen pendukung program.
3. Menentukan tata letak layout agar nantinya semua komponen – komponen yang sudah dipersiapkan bisa diaatur sedemikian rupa.
4. Event Handling dari sebuah aktivitas, seperti penekanan button, check box dan lain-lain.

**Java Swing** merupakan bagian dari JFC (*Java Foundation Classes*) yang menyediakan API untuk menangani hal yang berkaitan dengan GUI bagi program Java. Kita bisa membedakan komponen Swing dengan komponen AWT, di mana pada umumnya kelas-kelas yang berada dalam ***komponen Swing diawali dengan huruf J***, misal: **JButton, JLabel, JTextField, JRadioButton**.

## Percobaan
## Percobaan 1 - JFrame HelloGUI

Untuk membuat aplikasi Java berbasis GUI, kita butuh sebuah frame atau applet untuk media eksekusi aplikasi GUI. Pada Java sebuah frame dapat diwakili oleh sebuah kelas, yaitu JFrame. Melalui kelas JFrame kita bisa mendesain tampilan Java GUI sesuai kebutuhan. Beberapa method yang penting dan sering dipakai yaitu:

- setSize() : menentukan ukuran frame 
- setLocation() : menentukan lokasi frame pada bagian pojok kiri atas 
- setVisible() : menampilkan frame 
- setDefaultCloseOperation() : menentukan - operasi ketika frame ditutup 
- setLocationRelativeTo() : menentukan lokasi frame relatif terhadap parameter 
komponen yang diinputkan 
- Pack() : menentukan secara otomatis ukuran frame sesuai komponen yang 
dimasukkan

Implementasi ke dalam kode program, hasilnya adalah sebagai berikut:

> ![Percobaan 1](img/percobaan1.png)

Link kode program:

[HelloGui ](../../src/11_GUI/)

## Percobaan 2 - Menangani Input Pada GUI

Selanjutnya adalah bagaimana memanfaatkan komponen-komponen GUI pada Java untuk menangani inputan, dengan memanfaatkan JFrame, JButton, JLabel, JTextField, JPanel dsb.

Implementasi ke dalam kode program, hasilnya adalah sebagai berikut:

> ![Percobaan 2](img/percobaan2.png)

### Pertanyaan - Percobaan 2

1. Modifikasi kode program dengan menambahkan JButton baru untuk melakukan fungsi perhitungan penambahan, sehingga ketika button di klik (event click) maka akan menampilkan hasil penambahan dari nilai A dan B.

**Jawab:**

Setelah dimodifikasi, hasilnya adalah sebagai berikut :

> ![Percobaan 2](img/percobaan2.png)

Link kode program:

[MyInputForm ](../../src/11_GUI/)

## Percobaan 3 - Manajemen Layout

Java GUI menyediakan beberapa layout yang dapat digunakan pada program. Pada modul praktikum ini akan dijelaskan 3 contoh GUI layout, yaitu:

- Border layout 
- Grid layout 
- Box layout

Sebuah layout border dapat diilustrasikan pada Gambar dibawah, di mana kita dapat meletakkan komponen GUI pada lokasi tertentu misal utara, barat, tengah, timur, dan selatan. Untuk menentukan jenis layout yang akan digunakan, kita dapat memanggil method setLayout() dan selanjutnya memasukkan objek Border Layout.

> ![Percobaan 3](img/percobaan3.PNG)

Implementasi ke dalam kode program, hasilnya adalah sebagai berikut:

> ![Percobaan 3](img/percobaan3.png)

>Link kode program:

[Border ](../../src/11_GUI/)

[Box ](../../src/11_GUI/)

[Grid ](../../src/11_GUI/)

[LayoutGUI ](../../src/11_GUI/)

### Pertanyaan - Percobaan 3

1. Apa perbedaan dari Grid Layout, Box Layout dan Border Layout?

**Jawab:**
    Grid Layout 

2. Apakah fungsi dari masing-masing kode berikut?
```java
JFrame frame = new Border();
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
frame.setVisible(true);

JFrame frame2 = new Grid();
frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
frame2.setVisible(true);

JFrame frame3 = new Box();
frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
frame3.setVisible(true);
```

**Jawab:**



## Percobaan 4 -  Membuat GUI Melalui IDE Netbeans

Kali ini kita akan memulai membuat GUI pada aplikasi Apache Netbeans. Membuat project PercobaanGUI terlebih dahulu, lalu membuat file JFrame Form, lalu membuat file class dengan nama Swing.

Implementasi ke dalam kode program, hasilnya adalah sebagai berikut:


> ![Percobaan 4](img/percobaan4.png)

### Pertanyaan - Percobaan 4

1. Apakah fungsi dari kode berikut?
```java
java.awt.EventQueue.invokeLater(new Runnable() {
    public void run() {
        new Swing().setVisible(true);
    }
});
```

**Jawab:**



2. Mengapa pada bagian logika checkbox dan radio button digunakan multiple if ?

**Jawab:**



3. Lakukan modifikasi pada program untuk melakukan menambahkan inputan berupa alamat dan berikan fungsi pemeriksaan pada nilai Alamat tersebut jika belum diisi dengan menampilkan pesan peringatan

**Jawab:**

Setelah dimodifikasi, hasilnya adalah sebagai berikut :

> Source
> ![Percobaan 4](img/percobaan4_3.png)

> Design
> ![Percobaan 4](img/percobaan4_4.png)

Link kode program:

[Swing  (java)](../../src/11_GUI/)

[Swing  (form)](../../src/11_GUI/)

## Percobaan 5 - JTabPane, JTtree, JTable

Langkahnya hampir sama seperti percobaan sebelumnya (Percobaan 4). Kali ini kita akan membuat project baru dengan nama PercobaanGUI2, lalu membuat file class dengan nama Swing2.

Implementasi ke dalam kode program, hasilnya adalah sebagai berikut:

> ![Percobaan 4](img/percobaan4.png)

Link kode program:

[Swing2_  (java)](../../src/11_GUI/)

[Swing2_  (form)](../../src/11_GUI/)

### Pertanyaan - Percobaan 5

1. Apa kegunaan komponen swing JTabPane, JTtree, pada percobaan 5?

**Jawab:**



2. Modifikasi program untuk menambahkan komponen JTable pada tab Halaman 1 dan tab Halaman 2

**Jawab:**

Setelah dimodifikasi, hasilnya adalah sebagai berikut :

> (Halaman 1)

> ![Percobaan 4](img/percobaan5_1.png)

> (Halaman 2)

> ![Percobaan 4](img/percobaan5_3.png)

Link kode program:

[Swing2_  (java)](../../src/11_GUI/)

[Swing2_  (form)](../../src/11_GUI/)

## Tugas

Buatlah Sebuah Program yang mempunyai fungsi seperti kalkulator (mampu menjumlahkan, 
mengurangkan, mengalikan dan membagikan. Dengan tampilan seperti berikut:

> ![Tugas](img/tugas0.PNG)

Implementasi ke dalam kode program, hasilnya adalah sebagai berikut:

> Source

> Design
> ![Tugas](img/tugas2.png)

Link kode program : 

[Kalkulator  (java)](../../src/11_GUI/)

[Kalkulator  (form)](../../src/11_GUI/)

## Kesimpulan

> Pada jobsheet ke-11
> Mempelajari tentang bagaimana cara membuat GUI pada program java

## Pernyataan Diri

Saya menyatakan isi tugas, kode program, dan laporan praktikum ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.

Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.

Ttd,

***(Hafidh Sajid M.)***
