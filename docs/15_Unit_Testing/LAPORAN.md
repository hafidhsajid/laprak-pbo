# Laporan Praktikum #15 - Unit Testing
## Kompetensi

Setelah menyelesaikan lembar kerja ini mahasiswa diharapkan mampu:
1. Memahami konsep dan fungsi unit testing
2. Menerapkan unit testing dengan JUnit pada program sederhana.
3. Menerapkan unit testing dengan JUnit pada progam yang terkoneksi database
  
## Ringkasan Materi


## Percobaan 1 (Dasar Unit Testing)

percobaan ini adalah ilustrasi dari aplikasi pengiriman pesan 

>![1](img/satu.png)

>(Runner) : [link ke kode program](../../src/15_Unit_Testing/LearnUnitTesting1841720105Hafidh.java)


## Percobaan 2 (Unit Testing dengan Test Case)

pada percobaa ini akan dilaksanakan pembuatan test case dari praktikum 14


>![2](img/dua.png)



## Kesimpulan

pada praktikum ini dilakukannya program untuk melakukan unit testing pada praktikum 14 (GUI dan Database)

## Pernyataan Diri

Saya menyatakan isi tugas, kode program, dan laporan praktikum ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.

Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.

Ttd,

***(Hafidh Sajid M)***
