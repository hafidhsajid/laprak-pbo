# Laporan Praktikum #10 - Polimorfisme

## Kompetensi

Setelah melakukan percobaan pada jobsheet ini, diharapkan mahasiswa
mampu:
a. Memahami konsep dan bentuk dasar polimorfisme
b. Memahami konsep virtual method invication
c. Menerapkan polimorfisme pada pembuatan heterogeneous collection
d. Menerapkan polimorfisme pada parameter/argument method
e. Menerapkan object casting untuk meng-ubah bentuk objek



## Ringkasan Materi

dalam jobsheet ini kita akan mendapatkan pemahaman tentang :

* Virtual Method Invocation 
> Virtual method invocation terjadi ketika ada pemanggilan overriding method dari suatu objek polimorfisme. Disebut virtual karena antara method yang dikenali oleh compiler dan method yang dijalankan oleh JVM berbeda.

* Heterogeneous Collection
> Dengan adanya konsep polimorfisme, maka variabel array bisa dibuat heterogen. Artinya di dalam array tersebut bisa berisi berbagai macam objek yang berbeda

* Polymorphic Argument
> Polimorfisme juga bisa diterapkan pada argument suatu method. Tujuannyaagar method tersebut bisa menerima nilai argument dari berbagai bentuk objek.

* Operator Instanceof
> Operator instanceof bisa digunakan untuk mengecek apakah suatu objek merupakan hasil instansiasi dari suatu class tertentu. Hasil dari instanceof berupa nilai boolean.

* Object Casting
> Casting objek digunakan untuk mengubah tipe dari suatu objek. Jika ada suatu objek dari subclass kemudian tipenya diubah ke superclass, maka hal ini termasuk ke upcasting. Downcast terjadi jika ada suatu objek superclass, kemudian diubah menjadi objek dari subclass. Proses downcasting sering disebut juga sebagai explicit casting, karena bentuk tujuan dari casting harus dituliskan dalam tanda kurung, di depan objek yang akan di-casting.


## Percobaan

### Percobaan 1

Penjelasan :

* pengenalan dasar dari polimorfisme akan di lakukan pada percobaan kali ini, dimana parent / implement dari suatu objek bisa menjadi wadah dari anak-anak ataupun yang meng-implement pada class tersebut

Hasil 

![hasil](img/percobaan1.png)

Link kode program

1. Tester1 : [link ke kode program](../../src/10_Polimorfisme/praktikum/Tester1_1841720105Hafidh.java)

2. ElectricityBill : [link ke kode program](../../src/10_Polimorfisme/praktikum/ElectricityBill1841720105Hafidh.java)

3. Employee : [link ke kode program](../../src/10_Polimorfisme/praktikum/Employee1841720105Hafidh.java)

4. IntershipEmployee : [link ke kode program](../../src/10_Polimorfisme/praktikum/IntershipEmployee1841720105Hafidh.java)

5. IPayable : [link ke kode program](../../src/10_Polimorfisme/praktikum/Payable1841720105Hafidh.java)

6. PermanentEmployee : [link ke kode program](../../src/10_Polimorfisme/praktikum/PermanentEmployee1841720105Hafidh.java)

#### Pertanyaan

1. Class apa sajakah yang merupakan turunan dari class Employee?
2. Class apa sajakah yang implements ke interface Payable?
3. Perhatikan class Tester1, baris ke-10 dan 11. Mengapa e, bisa diisi dengan objek pEmp (merupakan objek dari class PermanentEmployee) dan objek iEmp (merupakan objek dari class InternshipEmploye) ?
4. Perhatikan class Tester1, baris ke-12 dan 13. Mengapa p, bisa diisi dengan objek pEmp (merupakan objek dari class PermanentEmployee) dan objek eBill (merupakan objek dari class ElectricityBill) ?
5. Coba tambahkan sintaks: p = iEmp; e = eBill; pada baris 14 dan 15 (baris terakhir dalam method main) ! Apa yang menyebabkan error?
6. Ambil kesimpulan tentang konsep/bentuk dasar polimorfisme!

#### Jawab

1. PermanentEmployee dan InternshipEmployee
2. ElectrycityBill dan PermanentEmployee
3. Karena pada class tersebut menerapkan konsep pewarisan sehingga objek tersebut bisa digunakan
4. Karena pada kedua objek tersebut terdapat implements terhadap interface Payable
5. pada syntak `p = iEmp;` pada class InternshipEmployee tidak terdapat implemt terhadap interface Payable sedangkan pada `e = eBill;` tidak terdapat pewarisan sifat dari class Employee
6. Class parent dan implements dapat dilakukan pewarisan sifat


### Percobaan 2

Penjelasan :

* kita akan di kenalkan dengan Virtual Method Invocation

Hasil 

![hasil](img/percobaan2.png)

Link kode program

1. (Tester2) : [link ke kode program](../../src/10_Polimorfisme/praktikum/Tester2_1841720105Hafidh.java)

#### Pertanyaan

1. Perhatikan class Tester2 di atas, mengapa pemanggilan e.getEmployeeInfo() pada baris 8 dan pEmp.getEmployeeInfo() pada baris 10 menghasilkan hasil sama?
2. Mengapa pemanggilan method e.getEmployeeInfo() disebut sebagai pemanggilan method virtual (virtual method invication), sedangkan pEmp.getEmployeeInfo() tidak?
3. Jadi apakah yang dimaksud dari virtual method invocation? Mengapa disebut virtual?

#### Jawab

1. Dikarenakan pada baris sebelumnya terdapat perintah yang membuat isi dari variabel e itu sama dengan class PermanentEmployee sehingga menghasilkan output yang sama
2. Hal ini terjadi dikarenakan objek e telah berubah menjadi pemp
3. disebut virtual karena merepresentasikan bentuk yang berbeda dari dirinya, namun tetap bisa menjadi dirinya sendiri bila tidak di ubah menjadi objek yang lain

### Percobaan 3

Penjelasan :

* penjelasan penggunaan array Virtual Method Invocation

Hasil 

![hasil](img/percobaan3.png)

Link kode program

1. (Tester3) : [link ke kode program](../../src/10_Polimorfisme/praktikum/Tester3_1841720105Hafidh.java)


#### Pertanyaan

1. Perhatikan array e pada baris ke-8, mengapa ia bisa diisi dengan objek-objek dengan tipe yang berbeda, yaitu objek pEmp (objek dari PermanentEmployee) dan objek iEmp (objek dari InternshipEmployee) ?
2. Perhatikan juga baris ke-9, mengapa array p juga biisi dengan objekobjek dengan tipe yang berbeda, yaitu objek pEmp (objek dari PermanentEmployee) dan objek eBill (objek dari ElectricityBilling) ?
3. Perhatikan baris ke-10, mengapa terjadi error?

#### Jawab

1. Dikarenakan array e adalah deklarasi dari class Employee sedangkan isi dari array yang ada adalah inheritance dari class Employee
2. Hal ini terjadi dan tidak terjadi error dikarenakan isi dari array p memiliki implements dari interface Payable sehingga hal tersebut bisa dijalanakan dan tidak terdapat error
3. Pada syntak `Employee e2[] = {pEmp, iEmp, eBill};` dapat terjadi error dikarenakan isi dari array e2 terdapat objek yang tidak memiliki pewarisan terhadap class Employee

### Percobaan 4

penjelasan :

* kita akan di kenalkan bentuk keseluruhan polimorfisme dari percobaan di bawah ini

Hasil 

![hasil](img/percobaan4.png)

Link kode program


1. Tester4 : [link ke kode program](../../src/10_Polimorfisme/praktikum/Tester4_1841720105Hafidh.java)
2. Owner : [link ke kode program](../../src/10_Polimorfisme/praktikum/Owner1841720105Hafidh.java)

#### Pertanyaan

1. Hal ini bisa terjadi dikarenakan interface merupakan interface dari Class ElectrycityBill dan Class PermanentEmployee
 
![](img/pertanyaan4no1.png)

2. Fungsi dari pemberian sesutau yang memiliki tipe Payable adalah untuk di dalam class owner adalah untuk mengimplementasi dari polimorphism 
3. Pada saat penambahan hal tersebut terjadi error dikarenakan pada  Class InternshipEmployee tdk mengimplementasi dari interface Payable sehingga terjadilah error
4. syntak `p instanceof ElectricityBill ` untuk melakukan pengecekan hubungan objek p apakah hasil instansiasi dari interface Payable terhadap class ElectricityBill
5. Untuk dikembalikan ke instansiasi sesungguhnya sehingga dapat memanggil method getBillInfo() yang hanya ada di class ElectricityBill
#### Jawab
1. Hal ini bisa terjadi dikarenakan interface merupakan interface dari Class ElectrycityBill dan Class PermanentEmployee
2. Fungsi dari pemberian sesutau yang memiliki tipe Payable adalah untuk di dalam class owner adalah untuk mengimplementasi dari polimorphism 
3. Pada saat penambahan hal tersebut terjadi error dikarenakan pada  Class InternshipEmployee tdk mengimplementasi dari interface Payable sehingga terjadilah error
4. syntak `p instanceof ElectricityBill ` untuk melakukan pengecekan hubungan objek p apakah hasil instansiasi dari interface Payable terhadap class ElectricityBill
5. Untuk dikembalikan ke instansiasi sesungguhnya sehingga dapat memanggil method getBillInfo() yang hanya ada di class ElectricityBill

### Tugas
Class Diagram Soal

![](img/diagramtugas.png)


1. tester : [link ke kode program](../../src/10_Polimorfisme/tugas/Tester1841720105Hafidh.java)

2. Destroyable : [link ke kode program](../../src/10_Polimorfisme/tugas/Destroyable1841720105Hafidh.java)

3. JumpingZombie : [link ke kode program](../../src/10_Polimorfisme/tugas/JumpingZombie1841720105Hafidh.java)

4. WalkingZombie : [link ke kode program](../../src/10_Polimorfisme/tugas/WalkingZombie1841720105Hafidh.java)

5. Plant : [link ke kode program](../../src/10_Polimorfisme/tugas/Plant1841720105Hafidh.java)

6. Barrier : [link ke kode program](../../src/10_Polimorfisme/tugas/Barrier1841720105Hafidh.java)

![](img/tugas.png)

## Kesimpulan

Dalam percobaan yang telah dilakukan lebih mendalam tentang apa itu Polimorphism dari bentuk, penggunaan fungsi dan lain lain sehingga untuk pemrograman bisa terlihat semakin enak dilihan dan efisien

## Pernyataan Diri

Saya menyatakan isi tugas, kode program, dan laporan praktikum ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.

Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.

Ttd,


***(Hafidh Sajid M)*** 
