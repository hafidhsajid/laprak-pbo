# Laporan Praktikum #4 - Relasi Kelas

## Kompetensi

Setelah menempuh pokok bahasan ini, mahasiswa mampu:
1. Memahami konsep relasi kelas;
2. Mengimplementasikan relasi has‑a dalam program.

## Ringkasan Materi

Pada kasus yang lebih kompleks, dalam suatu sistem akan ditemukan lebih dari satu class
yang saling memiliki keterkaitan antara class satu dengan yang lain. Pada percobaan‑percobaan 
sebelumnya, mayoritas kasus yang sudah dikerjakan hanya fokus pada satu class saja. Pada 
jobsheet ini akan dilakukan percobaan yang melibatkan beberapa class yang saling berelasi.
Misalnya terdapat class Laptop yang memiliki atribut berupa merk dan prosesor. Jika 
diperhatikan lebih rinci, maka atribut prosesor sendiri didalamnya memiliki data berupa merk, 
nilai cache memori, dan nilai clock‑nya. Artinya, ada class lain yang namanya Processor yang 
memiliki atribut merk, cache dan clock, dan atribut prosesor yang ada di dalam class Laptop itu 
merupakan objek dari class Proceessor tersebut. Sehingga terlihat antara class Laptop dan class 
Processor memiliki relasi (has‑a)

Jenis relasi has‑a ini yang akan dicontohkan pada percobaan di jobsheet ini. Apabila 
dilihat lebih rinci lagi, relasi tersebut disebut juga dengan agregasi (has-a). Relasi antar kelas 
yang lain adalah dependensi (uses-a) dan inheritance (is-a). Diperlukan inisiatif mandiri dari 
tiap mahasiswa untuk memperdalam jenis relasi lain terutama yang tidak dibahas pada mata
kuliah ini.

## Percobaan

### Percobaan 1:
1. Perhatikan diagram class berikut:
    
2. Buka project baru di Netbeans dan buat package dengan format berikut:
   <identifier>.relasiclass.percobaan1 (ganti dengan identitas anda atau nama 
   domain), Contoh: ac.id.polinema, jti.polinema, dan sebagainya)<br>
   ![satu](img/satu.png)<br>
   **Catatan:** Penamaan package dengan tambahan identifier untuk menghindari adanya 
   kemungkinan penamaan class yang bentrok.
3. Buatlah class Processor dalam package tersebut
    ```java
    public class Processor{
    
    }
    ```
4. Tambahkan atribut merk dan cache pada class Processor dengan akses modifier
   private .
    ```java
    private String merk;
    private double cache;
    ```
5. Buatlah _constructor_ default untuk class Processor.
6. Buatlah _constructor_ untuk class Processor dengan parameter merk dan cache.
7. Implementasikan *setter* dan *getter* untuk class Processor.
8. Implementasikan method info() seperti berikut:
    ```java
    public void info() {
    System.out.printf("Merk Processor = %s\n", merk);
    System.out.printf("Cache Memory = %.2f\n", cache);
    }
    ```
9. Kemudian buatlah class Laptop di dalam package yang telah anda buat.
10. Tambahkan atribut merk dengan tipe String dan proc dengan tipe Object Processor
    ```java
        private String merk;
        privete Processor proc;
    ```
11. Buatlah constructor default untuk class Laptop.
12. Buatlah constructor untuk class Laptop dengan parameter merk dan proc.
13. Selanjutnya implementasikan method info() pada class Laptop sebagai berikut
    ```java
    public void info() {
    System.out.println("Merk Laptop = " + merk);
    proc.info();
    }
    ```
14. Pada package yang sama, buatlah class MainPercobaan1 yang berisi method main().
15. Deklarasikan Object Processor dengan nama p kemudian instansiasi dengan informasi atribut Intel i5 untuk nilai merk serta 3 untuk nilai cache
```Processor p = new Processor("Intel i5",3);```
16. Kemudian deklarasikan serta instansiasi Objek Laptop dengan nama L dengan informasi 
    atribut Thinkpad dan Objek Processor yang telah dibuat
17. Panggil method info() dari Objek L.
`L.info();`
18. Tambahkan baris kode berikut
    ```java
    Processor p1 = new Processor();
    p1.setMerk("Intel i5");
    p1.setCache(4);
    Laptop L1 = new Laptop();
    L1.setMerk("Thinkpad");
    L1.setProc(p1);
    L1.info();
    ```
19.  Compile kemudian run class MainPercobaan1, akan didapatkan hasil seperti berikut:
<br>![satu](img/percobaansatu.png)<br>


link program : <br>
    
   [Source program Laptop](../../src/04_Relasi_Kelas/percobaan1/Laptop1841720105Hafidh.java) <br>
   [Source program Processor](../../src/04_Relasi_Kelas/percobaan1/Processor1841720105Hafidh.java) <br>
   [Main program](../../src/04_Relasi_Kelas/percobaan1/MainPercobaan_1_1841720105Hafidh.java) <br>
   
#### Pertanyaan
Berdasarkan percobaan 1, jawablah pertanyaan‐pertanyaan yang terkait:
1. Di dalam _class_ Processor dan _class_ Laptop , terdapat method _setter_ dan _getter_ untuk masing‐masing atributnya. Apakah gunanya _method setter_ dan _getter_ tersebut ?
<br>`Jawab: `<br>
fungsi dari _method setter_ dan _getter_ adalah untuk mengatur nilai dari variabel yang ada dan mengambil nilai dari sebuah variabel.

2. Di dalam _class_ Processor dan class Laptop, masing‐masing terdapat konstruktor default dan konstruktor berparameter. Bagaimanakah beda penggunaan dari kedua jenis
konstruktor tersebut ?
<br>`Jawab: `<br>
konstruktor berparameter adalah konstruktor yang memiliki parameter dan parameter tersebut digunakan untuk mengatur nilai awal sedangkan pada konstruktor default tidak mengatur nilai awal dari sebuah kelas

3. Perhatikan _class_ Laptop, di antara 2 atribut yang dimiliki (merk dan proc), atribut manakah yang bertipe object ?
<br>`Jawab: `<br>
yang bertipe objek adalah proc

4. Perhatikan _class_ Laptop, pada baris manakah yang menunjukan bahwa class Laptop
memiliki relasi dengan class Processor ?
<br>`Jawab: `<br>
Pada baris yang berisi kode:

    ```java
    private Processor1841720105Hafidh mProc;
    ```
    ```java
    public void setProcHafidh(Processor1841720105Hafidh mProc) {
        this.mProc = mProc;
    }
    ```

5. Perhatikan pada _class_ Laptop , Apakah guna dari sintaks `proc.info()` ?
<br>`Jawab: `<br>
sintak tersebut digunakan untuk menampilkan informasi laptop dan informasi dari processor

6. Pada _class_ MainPercobaan1, terdapat baris kode:
`Laptop l = new Laptop("Thinkpad", p);`
Apakah p tersebut ?
<br>`Jawab: `<br>
p adalah objek dari class processor 

Dan apakah yang terjadi jika baris kode tersebut diubah menjadi:
`Laptop l = new Laptop("Thinkpad", new Processor("Intel i5",3));`
Bagaimanakah hasil program saat dijalankan, apakah ada perubahan ?
<br>`Jawab: `<br>
Setelah program dijalankan tidak ada perubahan yang ada.
   
### Percobaan 2: Membuat dan mengakses anggota suatu class
 
Perhatikan diagram class berikut yang menggambarkan sistem rental mobil. Pelanggan bisa
menyewa mobil sekaligus sopir. Biaya sopir dan biaya sewa mobil dihitung per hari.
1. Tambahkan package <identifier>.relasiclass.percobaan2.
2. Buatlah class Mobil di dalam package tersebut.
3. Tambahkan atribut merk tipe String dan biaya tipe int dengan akses modifierprivate.
4. Tambahkan constructor default serta setter dan getter.
5. Implementasikan method hitungBiayaMobil
    ```java
    public int hitungBiayaMobil(int hari) {
    return biaya * hari;
    }
    ```
6. Tambahkan class Sopir dengan atribut nama tipe String dan biaya tipe int dengan akses
modifier private berikut dengan constructor default.
7. Implementasikan method hitungBiayaSopir
    ```java
    public int hitungBiayaSopir(int hari) {
    return biaya * hari;
    }
    ```
8. Tambahkan class Pelanggan dengan constructor default.
9. Tambahkan atribut‐atribut dengan akses modifier private berikut:
    | Atribut | Tipe   |
    |-----|-----|
    | nama    | String |
    | mobil   | Mobil  |
    | sopir   | Sopir  |
    | hari    | int    |
10. Implementasikan _setter_ dan _getter_.
11. Tambahkan method `hitungBiayaTotal`
    ```java
    public int hitungBiayaTotal() {
        return mobil.hitungBiayaMobil(hari) +
        sopir.hitungBiayaSopir(hari);
    }
    ```
12. Buatlah class MainPercobaan2 yang berisi method main(). Tambahkan baris kode
    berikut:
    ```java
    Mobil m = new Mobil();
    m.setMerk("Avanza");
    m.setBiaya(350000);
    Sopir s = new Sopir();
    s.setNama("John Doe");
    s.setBiaya(200000);
    Pelanggan p = new Pelanggan();
    p.setNama("Jane Doe");
    p.setMobil(m);
    p.setSopir(s);
    p.setHari(2);
    System.out.println("Biaya Total = " +
    p.hitungBiayaTotal());
    ```
 13. Compile dan jalankan class MainPercobaan2, dan perhatikan hasilnya!

 ![](img/dua.png)


link program : <br>
    

   [Source program Mobil](../../src/04_Relasi_Kelas/percobaan2/Mobil1841720105Hafidh.java)<br>
   [Source program Pelanggan](../../src/04_Relasi_Kelas/percobaan2/Pelanggan1841720105Hafidh.java)<br>
   [Source program Sopir](../../src/04_Relasi_Kelas/percobaan2/Sopir1841720105Hafidh.java)<br>
   [Main program](../../src/04_Relasi_Kelas/percobaan2/MainPercobaan2_1841720105Hafidh.java) <br>
   
#### Pertanyaan
1. Perhatikan class Pelanggan. Pada baris program manakah yang menunjukan bahwa class
   Pelanggan memiliki relasi dengan class Mobil dan class Sopir ?<br>
   `Jawab: `<br>
    pada baris program yang berisi: 

    ```java
    public int hitungBiayaTotal(){
        return mMobil.hitungmBiayaMobil(mHari)+
                mSopir.hitungmBiayaSopir(mHari);
    }
    ```

2. Perhatikan method hitungBiayaSopir pada class Sopir, serta method
   hitungBiayaMobil pada class Mobil. Mengapa menurut Anda method tersebut harus
   memiliki argument hari ?<br>
  `Jawab: `<br>
    Degan memiliki argumen hari maka untuk penghitungan biaya total dapat dilakukan karena jika argumen tersebut dihilangkan maka program tersebut tidak dapat berjalan karena di dalam method tersebut memiliki nilai return `mBiaya*mHari`
3. Perhatikan kode dari _class_ Pelanggan. Untuk apakah perintah
   `mobil.hitungBiayaMobil(hari)` dan `sopir.hitungBiayaSopir(hari)` ?
   <br>`Jawab: `<br>
    Perintah pada class mobil digunakan untuk menghitung Biaya total dari mobil sedangkan perintah pada class sopir digunakan untuk menghitung total biaya dari sopir yang digunakan.
4. Perhatikan _class_ MainPercobaan2. Untuk apakah sintaks `p.setMobil(m)` dan `p.setSopir(s)` ?
   <br>`Jawab: `<br>
    Sintak dari `p.setMobil(m)` digunakan untuk mengatur mobil sedangkan `p.setSopir(s)` untuk mengatur sopir yang akan digunakan
   
5. Perhatikan _class_ `MainPercobaan2`. Untuk apakah proses p.hitungBiayaTotal()
   tersebut ?
   <br>`Jawab: `<br>
   Proses diatas digunakan untuk menghitung total biaya yang harus dibayarkan oleh customer.

6. Perhatikan _class_ `MainPercobaan2`, coba tambahkan pada baris terakhir dari method
   main dan amati perubahan saat di‐run!
   `System.out.println(p.getMobil().getMerk());`
   <br>`Jawab: `<br>
   Setelah melakukan penambahan maka terjadi perubahan yaitu pada output layar di baris terakhir terdapat output yang berisi merk dari mobil yang digunakan.

### Percobaan 3
Pada percobaan‐percobaan sebelumnya, relasi dalam class dinyatakan dalam one‐to‐one. Tetapi ada
kalanya relasi class melibatkan lebih dari satu. Hal ini disebut dengan multiplicity. Untuk relasi
lebih rinci mengenai multiplicity, dapat dilihat pada tabel berikut.

| Multiplicity | Keterangan |
|:--:|:--|
| 0..1    | 0 atau 1 instance |
| 1   | Tepat 1 instance  |
| 0..*    | 0 atau lebih instance|
| 1..* | setidaknya 1 instance |
| n   | Tepat n instance (n diganti dengan sebuah angka) |
| m..n    | Setidaknya m instance, tetapi tidak lebih dari n |

1. Sebuah Kereta Api dioperasikan oleh Masinis serta seorang Asisten Masinis. Baik Masinis maupun Asisten Masinis keduanya merupakan Pegawai PT. Kereta Api Indonesia. Dari ilustrasi cerita tersebut, dapat digambarkan dalam diagram kelas sebagai berikut:
    <br>![tiga](img/percobaantiga.png)<br>
2. Perhatikan dan pahami diagram kelas tersebut, kemudian bukalah IDE anda!
3. Buatlah package `<identifier>.relasiclass.percobaan3`, kemudian tambahkan _class_ `Pegawai`.
4. Tambahkan atribut‐atribut ke dalam class Pegawai
```java
private String nip;
private String nama;

```
5.  Buatlah constructor untuk class Pegawai dengan parameter nip dan nama.
6. Tambahkan setter dan getter untuk masing‐masing atribut.
7. Implementasikan method info() dengan menuliskan baris kode berikut:
    ```java
    public String info() { String info = "";
    info += "Nip: " + this.nip + "\n";
    info += "Nama: " + this.nama + "\n";
    return info;
    }```
8. Buatlah _class_ KeretaApi berdasarkan diagram _class_.
9. Tambahkan atribut‐atribut pada class KeretaApi berupa _nama, kelas, masinis,_ dan
_asisten_.
    ```java
    private String nama;
    private String kelas;
    private Pegawai masinis;
    private Pegawai asisten;
    ```
10. Tambahkan constructor 3 parameter (nama, kelas, masinis) serta 4 parameter (nama,
kelas, masinis, asisten).
11. Tambahkan _setter_ dan _getter_ untuk atribut‐atribut yang ada pada _class_ KeretaApi .
12. Kemudian implementasikan _method_ info()
    ```java
    public String info() {
    String info = "";
    info += "Nama: " + this.nama + "\n";
    info += "Kelas: " + this.kelas + "\n";
    info += "Masinis: " + this.masinis.info() + "\n";
    info += "Asisten: " + this.asisten.info() + "\n";
    return info;
    }
    ```
13. Buatlah sebuah _class_ MainPercobaan3 dalam package yang sama.
14. Tambahkan _method_ main() kemudian tuliskan baris kode berikut.
    ```java
    Pegawai masinis = new Pegawai("1234", "Spongebob
    Squarepants");
    Pegawai asisten = new Pegawai("4567", "Patrick Star");
    KeretaApi keretaApi = new KeretaApi("Gaya Baru", "Bisnis",
    masinis, asisten);
    System.out.println(keretaApi.info());
    ```
   

![tiga](img/tiga.png)


   link program : <br>
    
   [Source program Kereta Api](../../src/04_Relasi_Kelas/percobaan3/KeretaApi1841720105Hafidh.java)<br>
   [Source program Pelanggan](../../src/04_Relasi_Kelas/percobaan3/Pegawai1841720105Hafidh.java)<br>
   [Main program](../../src/04_Relasi_Kelas/percobaan3/MainPercobaan3_1841720105Hafidh.java) <br>

#### Pertanyaan

1. Di dalam _method_ `info()` pada _class_ `KeretaApi`, baris `this.masinis.info()` dan
`this.asisten.info()` digunakan untuk apa ?
<br>`Jawab: `<br>
Baris `this.masinis.info()` digunakan untuk mengeluarkan info dari masinis sedangkan pada baris yang berisi `this.asisten.info()` digunakan untuk mengeluarkan informasi dari asisten

2. Buatlah _main_ program baru dengan nama _class_ `MainPertanyaan` pada _package_ yang
sama. Tambahkan kode berikut pada _method_ main()!
    ```java
    Pegawai masinis = new Pegawai("1234", "Spongebob
    Squarepants");
    KeretaApi keretaApi = new KeretaApi("Gaya Baru", "Bisnis",
    masinis);
    System.out.println(keretaApi.info());
    ```
    
3. Apa hasil output dari main program tersebut ? Mengapa hal tersebut dapat terjadi ?
<br>`Jawab: `<br>
Yang terjadi pada setelah pembuatan class diatas keluar output yang berisikan error null pointer exception hal ini terjadi karena objek dari `asisten` tidak ada 
4. Perbaiki class KeretaApi sehingga program dapat berjalan !
<br>`Jawab: `<br>
    ```java
    Pegawai1841720105Hafidh masinis = new Pegawai1841720105Hafidh("1234", "Spongebob Squarepants");
            Pegawai1841720105Hafidh asisten = new Pegawai1841720105Hafidh("4567", "Patrick Star");
            KeretaApi1841720105Hafidh keretaApi = new KeretaApi1841720105Hafidh("Gaya Baru", "Bisnis",
            masinis, asisten);
            System.out.println(keretaApi.info());
    ```

### Percobaan 4

1. Perhatikan dan pahami diagram _class_ tersebut.
![empat](img/percobaanempat.png)
2. Buatlah masing‐masing _class_ Penumpang, Kursi dan Gerbong sesuai rancangan
tersebut pada package `<identifier>.relasiclass.percobaan4.`
3. Tambahkan _method_ info() pada _class_ Penumpang
    ```java
    public String info() {
    String info = "";
    info += "Ktp: " + ktp + "\n";
    info += "Nama: " + nama + "\n";
    return info;
    }```

4. Tambahkan _method_ info() pada _class_ Kursi
    ```java
    public String info() {
    String info = "";
    info += "Nomor: " + nomor + "\n";
    if (this.penumpang != null) {
    info += "Penumpang: " + penumpang.info() + "\n";
    }
    return info;
    }
    ```
5. Pada _class_ Gerbong buatlah _method_ initKursi() dengan akses private.
    ```java
    private void initKursi() {
    for (int i = 0; i < arrayKursi.length; i++) {
    this.arrayKursi[i] = new Kursi(String.valueOf(i + 1));
    }
    }```
6. Panggil _method_ initKursi() dalam _constructor_ Gerbong sehingga baris kode menjadi
berikut:

    ```java
    public Gerbong(String kode, int jumlah) {
    this.kode = kode;
    this.arrayKursi = new Kursi[jumlah];
    this.initKursi();
    }
    ```

7. Tambahkan _method_ info() pada _class_ Gerbong
    ```java
    public String info() {
    String info = "";
    info += "Kode: " + kode + "\n";
    for (Kursi kursi : arrayKursi) {
    info += kursi.info();
    }
    return info;
    }```
8. Implementasikan _method_ untuk memasukkan penumpang sesuai dengan nomor kursi.
    ```java
    public void setPenumpang(Penumpang penumpang, int nomor) {
    this.arrayKursi[nomor - 1].setPenumpang(penumpang);
    }```
9. Buatlah _class_ MainPercobaan4 yang berisi _method_ main(). Kemudian tambahkan
baris berikut!

    ```java
    Penumpang p = new Penumpang("12345", "Mr. Krab");
    Gerbong gerbong = new Gerbong("A", 10);
    gerbong.setPenumpang(p, 1);
    System.out.println(gerbong.info());
    ```

![empat](img/empat.png)
   
link program : <br>
    
   [Source program Gerbong](../../src/04_Relasi_Kelas/percobaan4/Gerbong1841720105Hafidh.java)<br>
   [Source program Kursi](../../src/04_Relasi_Kelas/percobaan4/Kursi1841720105Hafidh.java)<br>
   [Source program Penumpang](../../src/04_Relasi_Kelas/percobaan4/Penumpang1841720105Hafidh.java)<br>
   [Main program](../../src/04_Relasi_Kelas/percobaan4/MainPercobaan4_1841720105Hafidh.java) <br>

#### Pertanyaan

1. Pada _main_ program dalam _class_ MainPercobaan4, berapakah jumlah kursi dalam
Gerbong A ?
<br>`Jawab: `<br>
Jumlah kursi yang ada adalah 10 kursi 

2. Perhatikan potongan kode pada method `info() `dalam class Kursi. Apa maksud kode
tersebut ?

    ```java
    if (this.penumpang != null) {
    info += "Penumpang: " + penumpang.info() + "\n";
    } 
    ```
<br>`Jawab: `<br>
Pada method tersebut yang berada di dalam class kursi memiliki makna yaitu jika objek penumpang adalah memiliki isi atau tidak kosong maka sintak ``info += "Penumpang: " + penumpang.info() + "\n";`` akan tereksekusi

3. Mengapa pada _method_ `setPenumpang()` dalam _class_ Gerbong, nilai nomor dikurangi
dengan angka 1 ?
<br>`Jawab: `<br>
Karena deklarasi method initKursi memulai array pada bilangan ke-0.

4. Instansiasi objek baru budi dengan tipe _Penumpang_, kemudian masukkan objek baru
tersebut pada gerbong dengan `gerbong.setPenumpang(budi, 1)`. Apakah yang
terjadi ?
<br>`Jawab: `<br>
Yang terjdi adalah nama penumpang yang sebelumnya "Mr. Crab " berubah menjadi "Budi"

5. Modifikasi program sehingga tidak diperkenankan untuk menduduki kursi yang sudah ada
penumpang lain !
<br>`Jawab: `<br>

```java
public static void main(String[] args) {
        Penumpang1841720105Hafidh p = new Penumpang1841720105Hafidh("1234", "Mr. Krab");
        Penumpang1841720105Hafidh budi = new Penumpang1841720105Hafidh("134", "Budi");
        Gerbong1841720105Hafidh gerbong = new Gerbong1841720105Hafidh("A", 10);
        gerbong.setPenumpang(p, 1);
        gerbong.setPenumpang(budi, 2);
        System.out.println(gerbong.info());
    }
```

### Tugas
Buatlah sebuah studi kasus, rancang dengan class diagram, kemudian implementasikan ke dalam
program! Studi kasus harus mewakili relasi class dari percobaan‐percobaan yang telah dilakukan
pada materi ini, setidaknya melibatkan minimal 4 class (class yang berisi main tidak dihitung).


<br>![diagram](img/Diagram.png) <br>

<br>![tugas](img/tugas.png)<br>

link program : <br>
    
[Source program Jenis](../../src/04_Relasi_Kelas/tugas/Jenis1841720105Hafidh.java)<br>
[Source program Loot](../../src/04_Relasi_Kelas/tugas/Loot1841720105Hafidh.java)<br>
[Source program Monster](../../src/04_Relasi_Kelas/tugas/Monster1841720105Hafidh.java)<br>
[Source program Tingkatan](../../src/04_Relasi_Kelas/tugas/Tingkatan1841720105Hafidh.java)<br>
[Main program](../../src/04_Relasi_Kelas/tugas/Main1841720105Hafidh.java) <br>


   
## Kesimpulan

Percobaan diatas lebih mendalami tentang bagaimana pembuatan relasi antar kelas

## Pernyataan Diri

Saya menyatakan isi tugas, kode program, dan laporan praktikum ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.

Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.

Ttd,

***Hafidh Sajid Malik***
