# Laporan Praktikum #6 -Overriding dan Overloading

## Kompetensi
Setelah menempuh pokok bahasan ini, mahasiswa mampu :

1. Memahami konsep overloading dan overriding,
2. Memahami perbedaan overloading dan overriding,
3. Ketepatan dalam mengidentifikasi method overriding dan overloading
4. Ketepatan dalam mempraktekkan instruksi pada jobsheet 
5. Mengimplementasikan method overloading dan overriding.

## Ringkasan Materi


## Percobaan

### Percobaan 1
 
Untuk kasus contoh berikut ini, terdapat tiga kelas, yaitu Karyawan, Manager, dan Staff.
Class Karyawan merupakan superclass dari Manager dan Staff dimana subclass Manager dan
Staff memiliki method untuk menghitung gaji yang berbeda.

![1](img/uml%20praktikum.png)

##### Karyawan
```java

public class Karyawan1841720105Hafidh {
    private String mNama;
    private String mNip;
    private String mGolongan;
    private double mGaji;

    public String getmNamaHafidh() {
        return mNama;
    }

    public String getmNipHafidh() {
        return mNip;
    }

    public String getmGolonganHafidh() {
        return mGolongan;
    }

    public double getmGajiHafidh() {
        return mGaji;
    }

    public void setmNamaHafidh(String mNama) {
        this.mNama = mNama;
    }

    public void setmNipHafidh(String mNip) {
        this.mNip = mNip;
    }

    public void setmGolonganHafidh(String mGolongan) {
        this.mGolongan = mGolongan;
        switch (mGolongan.charAt(0)){
            case '1' : this.mGaji = 5000000;
            break;
            case '2' : this.mGaji = 3000000;
            break;
            case '3' : this.mGaji = 2000000;
            break;
            case '4' : this.mGaji = 1000000;
            break;
            case '5' : this.mGaji = 750000;
            break;
        }
    }

    public void setmGajiHafidh(double mGaji) {
        this.mGaji = mGaji;
    }
}
``` 

#### Staff

```java
public class Staff1841720105Hafidh extends Karyawan1841720105Hafidh {
    private int mLembur;
    private double mGajiLembur;

    public int getLemburHafidh() {
        return mLembur;
    }

    public void setLemburHafidh(int lembur) {
        this.mLembur = lembur;
    }

    public double getGajiLemburHafidh() {
        return mGajiLembur;
    }

    public void setGajiLemburHafidh(double gajiLembur) {
        this.mGajiLembur = gajiLembur;
    }

    public double getGajiHafidh(int lembur, double gajiLembur) {
        return super.getmGajiHafidh()+lembur*gajiLembur;
    }

    @Override
    public double getmGajiHafidh() {
        return super.getmGajiHafidh()* mGajiLembur;
    }

    public void lihatInfoHafidh(){
        System.out.println("NIP         : "+this.getmNipHafidh());
        System.out.println("Nama        : "+this.getmNamaHafidh());
        System.out.println("Gologan     : "+this.getmGolonganHafidh());
        System.out.println("Jml Lembur  : "+this.getLemburHafidh());
        System.out.printf("Gaji Lembur :%.0f\n "+this.getGajiLemburHafidh());
        System.out.printf("Gaji        :%.0f\n "+this.getmGajiHafidh());
    }
}
```

#### Manager

```java
public class Manager1841720105Hafidh extends Karyawan1841720105Hafidh {
    private double mTunjangan;
    private String mBagian;
    private Staff1841720105Hafidh st[];

    public double getmTunjanganHafidh() {
        return mTunjangan;
    }

    public void setmTunjanganHafidh(double mTunjangan) {
        this.mTunjangan = mTunjangan;
    }

    public String getmBagianHafidh() {
        return mBagian;
    }

    public void setmBagianHafidh(String mBagian) {
        this.mBagian = mBagian;
    }

    public void setStaffHafidh(Staff1841720105Hafidh[] st) {
        this.st = st;
    }
    public void viewStaffHafidh(){
        int i;
        System.out.println("-------------------");
        for (i = 0; i < st.length; i++) {
            st[i].lihatInfoHafidh();
        }
        System.out.println("------------------");
    }
    public void lihatInfoHafidh(){
        System.out.println("Manager1841720105Hafidh     : "+this.getmBagianHafidh());
        System.out.println("NIP                         : "+this.getmNipHafidh());
        System.out.println("Nama                        : "+this.getmNamaHafidh());
        System.out.println("Gologan                     : "+this.getmGolonganHafidh());
        System.out.printf("Tunjangan                   : %.0f\n",this.getmTunjanganHafidh());
        System.out.printf("Gaji                        : %.0f\n",this.getmGajiHafidh());
        System.out.println("Bagian                      : "+this.getmBagianHafidh());
    }

    @Override
    public double getmGajiHafidh() {
        return super.getmGajiHafidh()+ mTunjangan;
    }
}
```

#### Utama

```java
public class Utama1841720105Hafidh {
    public static void main(String[] args) {
        System.out.println("Program Testing Class Manager1841720105Hafidh & Staff1841720105Hafidh");
        Manager1841720105Hafidh man[] = new Manager1841720105Hafidh[2];
        Staff1841720105Hafidh staff1[] = new Staff1841720105Hafidh[2];
        Staff1841720105Hafidh staff2[] = new Staff1841720105Hafidh[3];

        man[0]=new Manager1841720105Hafidh();
        man[0].setmNamaHafidh("Tedjo");
        man[0].setmNipHafidh("101");
        man[0].setmGolonganHafidh("1");
        man[0].setmTunjanganHafidh(500000);
        man[0].setmBagianHafidh("Administrasi");

        man[1]=new Manager1841720105Hafidh();
        man[1].setmNamaHafidh("Atika");
        man[1].setmNipHafidh("102");
        man[1].setmGolonganHafidh("1");
        man[1].setmTunjanganHafidh(250000);
        man[1].setmBagianHafidh("Pemasaran");

        staff1[0]=new Staff1841720105Hafidh();
        staff1[0].setmNamaHafidh("Usman");
        staff1[0].setmNipHafidh("0003");
        staff1[0].setmGolonganHafidh("2");
        staff1[0].setLemburHafidh(10);
        staff1[0].setGajiLemburHafidh(10000);

        staff1[1]=new Staff1841720105Hafidh();
        staff1[1].setmNamaHafidh("Anugrah");
        staff1[1].setmNipHafidh("0005");
        staff1[1].setmGolonganHafidh("2");
        staff1[1].setLemburHafidh(10);
        staff1[1].setGajiLemburHafidh(55000);
        man[0].setStaffHafidh(staff1);

        staff2[0]=new Staff1841720105Hafidh();
        staff2[0].setmNamaHafidh("Hendra");
        staff2[0].setmNipHafidh("0004");
        staff2[0].setmGolonganHafidh("3");
        staff2[0].setLemburHafidh(15);
        staff2[0].setGajiLemburHafidh(5500);

        staff2[1]=new Staff1841720105Hafidh();
        staff2[1].setmNamaHafidh("Arie");
        staff2[1].setmNipHafidh("0006");
        staff2[1].setmGolonganHafidh("4");
        staff2[1].setLemburHafidh(5);
        staff2[1].setGajiLemburHafidh(100000);

        staff2[2]=new Staff1841720105Hafidh();
        staff2[2].setmNamaHafidh("Mentari");
        staff2[2].setmNipHafidh("0007");
        staff2[2].setmGolonganHafidh("3");
        staff2[2].setLemburHafidh(6);
        staff2[2].setGajiLemburHafidh(20000);
        man[1].setStaffHafidh(staff2);

        man[0].lihatInfoHafidh();
        man[1].lihatInfoHafidh();

    }
}
```
Link kode program 

> [Staff](../../src/07_Overloading%20dan%20Overriding/praktikum/Staff1841720105Hafidh.java)

> [Manager](../../src/07_Overloading%20dan%20Overriding/praktikum/Manager1841720105Hafidh.java)

> [Karyawan](../../src/07_Overloading%20dan%20Overriding/praktikum/Karyawan1841720105Hafidh.java)

> [Utama](../../src/07_Overloading%20dan%20Overriding/praktikum/Utama1841720105Hafidh.java)



### Latihan

```java
    public class PerkalianKu1841720105Hafidh {
        void perkalianHafidh(int a, int b){
            System.out.println(a*b);
        }
        void perkalianHafidh(int a, int b, int c){
            System.out.println(a*b*c);
        }
    
        public static void main(String[] args) {
            PerkalianKu1841720105Hafidh objek = new PerkalianKu1841720105Hafidh();
    
            objek.perkalianHafidh(25,43);
            objek.perkalianHafidh(34, 23, 56);
    
        }
    }
```
    
1. Dari source coding diatas terletak dimanakah overloading?

    **jawab**
    
    Letak dri drioverloading pada source code diatas terletak pada
    
    ```java
    void perkalianHafidh(int a, int b){
                System.out.println(a*b);
            }
            void perkalianHafidh(int a, int b, int c){
                System.out.println(a*b*c);
            }
    ```

2. Jika terdapat overloading ada berapa jumlah parameter yang berbeda?
    
    **Jawab**
    
    Terdapat satu parameter yang berbeda pada source code diatas

    ```java
    public class PerkalianKu1841720105Hafidh {
        void perkalianHafidh(int a, int b){
            System.out.println(a*b);
        }
        void perkalianHafidh(double a, double b){
            System.out.println(a*b);
        }
    
        public static void main(String[] args) {
            PerkalianKu1841720105Hafidh objek = new PerkalianKu1841720105Hafidh();
    
            objek.perkalianHafidh(25,43);
            objek.perkalianHafidh(34.56, 23.7);
    
        }
    }
    ```

3.  Dari source coding diatas terletak dimanakah overloading?

    **Jawab**
    
    Overloading terdapat pada source code
    ```java
    void perkalianHafidh(int a, int b){
            System.out.println(a*b);
        }
        void perkalianHafidh(double a, double b){
            System.out.println(a*b);
        }
    ``` 

4.  Jika terdapat overloading ada berapa tipe parameter yang berbeda?

    **Jawab**
    
    Terdapat dua parameter berbeda

    ```java
    class ikan {
        public void swimHafidh(){
            System.out.println("ikan bisa berenang");
        }
    }
    class piranha extends ikan {
        public void swimHafidh(){
            System.out.println("piranha bisa makan daging");
        }
    }
    
    public class Fish1841720105Hafidh {
        public static void main(String[] args) {
            ikan a = new ikan();
            ikan b = new piranha();
            a.swimHafidh();
            b.swimHafidh();
        }
    }
    ```
5. Dari source coding diatas terletak dimanakah overriding?

    **Jawab**
    
    Dari source code diatas yang termasuk overriding adalah
    ```java
    class ikan {
        public void swimHafidh(){
            System.out.println("ikan bisa berenang");
        }
    }
    class piranha extends ikan {
        public void swimHafidh(){
            System.out.println("piranha bisa makan daging");
        }
    }
    ```
    
6. Jabarkanlah apabila sourcoding diatas jika terdapat overriding?

    **Jawab**
    
    Pada source code diatas terdapat class Ikan,  memiliki method swim() dan tidak memiliki parameter kemudian pada class piranha terdapat kata kunci extends yang menangdakan memiliki penurunan sifat dari Ikan  kemudian didalam class piranha terdapat method swim() yang mengoveride dari class Ikan sehingga yang keluar di dalam output adalah method dari class piranha bukan dari class Ikan

### Tugas

#### Overloading

Implementasikan konsep overloading pada class diagram dibawah ini :

![1](img/tugasmanusiauml.png)

##### Hasil 

![hasil](img/tugasmanusia.png)

Link kode program
 
>[Main](../../src/07_Overloading%20dan%20Overriding/tugas/manusia/main1841720105Hafidh.java)

>[Dosen](../../src/07_Overloading%20dan%20Overriding/tugas/manusia/Dosen1841720105Hafidh.java)

>[Mahasiswa](../../src/07_Overloading%20dan%20Overriding/tugas/manusia/Mahasiswa1841720105Hafidh.java)

>[Manusia](../../src/07_Overloading%20dan%20Overriding/tugas/manusia/Manusia1841720105Hafidh.java)


#### Overriding

Implementasikan class diagram dibawah ini dengan menggunakan teknik dynamic method dispatch :

![2](img/tugassegitigauml.png)

##### Hasil
![hasil2](img/tugassegitiga.png)

Link kode program 

[Segitiga](../../src/07_Overloading%20dan%20Overriding/tugas/segitiga/Segitiga1841720105Hafidh.java)


## Kesimpulan

Dalam percobaan ini mempelajari tentang overload dan overriding 

## Pernyataan Diri

Saya menyatakan isi tugas, kode program, dan laporan praktikum ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.

Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.

Ttd,


***(Hafidh Sajid M)*** 
