# Laporan Praktikum #9 -Abstract Class dan Interface

## Kompetensi
Setelah menyelesaikan lembar kerja ini mahasiswa diharapkan mampu:
1. Menjelaskan maksud dan tujuan penggunaan Abstract Class;
2. Menjelaskan maksud dan tujuan penggunaan Interface;
3. Menerapkan Abstract Class dan Interface di dalam pembuatan program.

## Percobaan

### Percobaan 1

Hasil 

![hasil](img/percobaansatu.png)

Link kode program

> [Hewan](../../src/09_Abstract_Class_dan_interface/abstractclass/Hewan1841720105Hafidh.java)

> [Ikan](../../src/09_Abstract_Class_dan_interface/abstractclass/Ikan1841720105Hafidh.java)

> [Kucing](../../src/09_Abstract_Class_dan_interface/abstractclass/Kucing1841720105Hafidh.java)

> [Orang](../../src/09_Abstract_Class_dan_interface/abstractclass/Orang1841720105Hafidh.java)

> [Program](../../src/09_Abstract_Class_dan_interface/abstractclass/Program1841720105Hafidh.java)

#### Pertanyaan

Bolehkah apabila sebuah class yang meng-extend suatu abstract class tidak mengimplementasikan method abstract yang ada di class induknya? Buktikan!

Tidak bisa

![a](img/pertanyaansatu.png)


### Percobaan 2

Hasil 

![hasil](img/percobaandua.png)

Link kode program

>[Program](../../src/09_Abstract_Class_dan_interface/interfacelatihan/Program1841720105Hafidh.java)
>
>[ICumlaude](../../src/09_Abstract_Class_dan_interface/interfacelatihan/ICumlaude1841720105Hafidh.java)
>
>[Mahasiswa](../../src/09_Abstract_Class_dan_interface/interfacelatihan/Mahasiswa1841720105Hafidh.java)
>
>[PascaSarjana](../../src/09_Abstract_Class_dan_interface/interfacelatihan/PascaSarjana1841720105Hafidh.java)
>
>[Rektor](../../src/09_Abstract_Class_dan_interface/interfacelatihan/Rektor1841720105Hafidh.java)
>
>[Sarjana](../../src/09_Abstract_Class_dan_interface/interfacelatihan/Sarjana1841720105Hafidh.java)

#### Pertanyaan
1. Mengapa pada langkah nomor 9 terjadi error? Jelaskan!
2. Dapatkah method kuliahDiKampus() dipanggil dari objek sarjanaCumlaude di class
Program? Mengapa demikian?
3. Dapatkah method kuliahDiKampus() dipanggil dari parameter mahasiswa di method
beriSertifikatCumlaude() pada class Rektor? Mengapa demikian?
4. Modifikasilah method beriSertifikatCumlaude() pada class Rektor agar hasil eksekusi
class Program menjadi seperti berikut ini:

#### Jawab

1. karena class mahasiswa tidak mengimplementasi interface ICumlaude, tetapi di dalam class rektor memiliki parameter tipe data ICumlaude, sehingga terjadi error
2. Bisa karena di dalam class sarjana merupakan inheritance dari class Mahasiswa sehingga method tersebut bisa dipanggil
3. Bisa karena Rektor memiliki hubungan dengan interface ICumlaude sehinggal method tersebut dapat dieksekusi dengan baik
4. Dengan menambahkan code seperti gambar dibawah
![](img/pertanyaandua.png)

### Percobaan 3

Hasil 

![hasil](img/percobaantiga.png)

Link kode program

>[Sarjana](../../src/09_Abstract_Class_dan_interface/praktikum3/Sarjana1841720105Hafidh.java)
>
>[Rektor](../../src/09_Abstract_Class_dan_interface/praktikum3/Rektor1841720105Hafidh.java)
>
>[PascaSarjana](../../src/09_Abstract_Class_dan_interface/praktikum3/PascaSarjana1841720105Hafidh.java)
>
>[Mahasiswa](../../src/09_Abstract_Class_dan_interface/praktikum3/Mahasiswa1841720105Hafidh.java)
>
>[ICumlaude](../../src/09_Abstract_Class_dan_interface/praktikum3/ICumlaude1841720105Hafidh.java)
>
>[Program](../../src/09_Abstract_Class_dan_interface/praktikum3/Program1841720105Hafidh.java)
>
>[IBerprestasi](../../src/09_Abstract_Class_dan_interface/praktikum3/IBerprestasi1841720105Hafidh.java)
>

#### Pertanyaan
Apabila Sarjana Berprestasi harus menjuarai kompetisi NASIONAL dan menerbitkan
artikel di jurnal NASIONAL, maka modifikasilah class-class yang terkait pada aplikasi
Anda agar di class Program objek pakRektor dapat memberikan sertifikat mawapres pada
objek sarjanaCumlaude.

#### Jawab
![](img/pertanyaantiga.png)
dengan menambahakan source code sperti gambar di atas maka akan keluar akan berhasil

## Kesimpulan

Dalam percobaan ini mempelajari tentang overload dan overriding 

## Pernyataan Diri

Saya menyatakan isi tugas, kode program, dan laporan praktikum ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.

Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.

Ttd,


***(Hafidh Sajid M)*** 
