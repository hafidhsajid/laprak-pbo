# Laporan Praktikum #2 - Class dan Objek

## Kompetensi

Mahasiswa dapat memahami deskripsi dari class dan object

- Mahasiswa memahami implementasi dari class
- Mahasiswa dapat memahami implementasi dari attribute
- Mahasiswa dapat memahami implementasi dari method
- Mahasiswa dapat memahami implementasi dari proses instansiasi
- Mahasiswa dapat memahami implementasi dari try-catch
- Mahasiswa dapat memahami proses pemodelan class diagram menggunakan UML

## Ringkasan Materi

(berisi catatan penting pribadi selama praktikum berlangsung ataupun menemukan permasalahan khusus saat melakukan percobaan)

## Percobaan

### Percobaan 1: Membuat Class Diagram

Studi Kasus 1: <br>
Dalam suatu perusahaan salah satu data yang diolah adalah data karyawan. Setiap karyawan memiliki id, nama, jenis kelamin, jabatan, jabatan, dan gaji. Setiap mahasiswa juga bisa menampilkan data diri pribadi dan melihat gajinya

1. Gambarkan desain class diagram dari studi kasus 1!
<br>![pecobaan 1](img/percobaan1.1.png)
2. Sebutkan Class apa saja yang bisa dibuat dari studi kasus 1.! <br>
`Class yang bisa dibuat pada kasus 1 adalah Karyawan()`
3. Sebutkan atribut beserta tipe data yang dapat diidentifikasi dari masing-masing class dari studi kasus1! <br>

    Karyawan:
    - Id : int
    - Nama : String
    - jenis_kelamin : String
    - jabatan : String
    - gaji : Double <br><br>

4. Sebutkan method-method yang sudah anda buat dari masing-masing class pada studi kasus 1! <br>
    
    Karyawan:
    - show_data() : void
    - show_gaji() : void

### Percobaan 2: Membuat dan mengakses anggota suatu class

Studi Kasus 2: <br>
Perhatikan class diagram dibawah ini. <br>
![sk2](img/percobaan2_soal.png)

Langkah kerja:

1. Bukalah text editor atau IDE, misalnya Notepad ++ / netbeans
2. Ketikkan kode program berikut ini: <br>
![2](img/percobaan2_soal2.png)
3. Simpan dengan nama file Mahasiswa.java.
4. Untuk dapat mengakses anggota-anggota dari suatu obyek, maka harus dibuat instance dari class tersebut terlebih dahulu. Berikut ini adalah cara pengaksesan anggota-anggota dari class Mahasiswa dengan membuka file baru kemudian ketikkan kode program berikut:
![2](img/percobaan2_soal3.png)

5. Simpan file dengan TestMahasiswa.java
6. Jalankan class TestMahasiswa

7. Jelaskan pada bagian mana proses pendeklarasian atribut pada program diatas!<br>
`Pendeklarasian attribut dari kode diatas adalah terdapat pada perintah:`

 ```
java
    - public int nim;
    - public String nama;
    - public String alamat;
    - public String kelas;
```

8. Jelaskan pada bagian mana proses pendeklarasian method pada program diatas!
`Pendeklarasian method dari kode diatas adalah terdapat pada perintah:`

 ```
java
 - public void tampilBiodata(){
```

9. Berapa banyak objek yang di instansiasi pada program diatas!

10. Apakah yang sebenarnya dilakukan pada sintaks program “mhs1.nim=101” ? <br>
`Yang sebenarnya terjadi pada sintaks program diatas yaitu adalah mengatur variabel nim yang berada pada kelas Mahasiswa menjadi 101`
11. Apakah yang sebenarnya dilakukan pada sintaks program “mhs1.tampilBiodata()” ? <br>
`Pada sintaks tersebut terjadi pengeluaran output biodata dari program yang telah dibuat`
12. Instansiasi 2 objek lagi pada program diatas!

![2](img/percobaan2.png)

[Source program](../../src/02_Class_dan_Object/praktikum/Mahasiswa.java) <br>
[Main program](../../src/02_Class_dan_Object/praktikum/TestMahasiswa.java) <br>

### Percobaan 3:  Menulis method yang memiliki argument/parameter dan memiliki return

Langkah kerja:

1. Bukalah text editor atau IDE, misalnya Notepad ++ / netbeans.
2. Ketikkan kode program berikut ini: <br>
![soal1](img/percobaan3_soal1.png)
3. Simpan dengan nama file Barang.java
4. Untuk dapat mengakses anggota-anggota dari suatu obyek, maka harus dibuat instance dari class tersebut terlebih dahulu. Berikut ini adalah cara pengaksesan anggota-anggota dari class Barang dengan membuka file baru kemudian ketikkan kode program berikut:<br>
![soal2](img/percobaan3_soal2.png)
5. Simpan dengan nama file TestBarang.java
6. Jalankan program tersebut!
7. Apakah fungsi argumen dalam suatu method?<br>
`Fungsi dari argumen dalam suatu method adalah`
8. Ambil kesimpulan tentang kegunaan dari kata kunci return , dan kapan suatu method harus memiliki return!<br>
```
Kata kunci return digunakan untuk memanggil hasil dari fungsi yang telah dijalankan, 
kata kunci return digunakan pada metode selain void sedangkan untuk metode yang berjenis void tidak harus memiliki kata kunci return
```

![3](img/percobaan3.png)

[Source program](../../src/02_Class_dan_Object/praktikum/Barang.java) <br>
[Main program](../../src/02_Class_dan_Object/praktikum/TestBarang.java) <br>

## TUGAS

1. Suatu toko persewaan video game salah satu yang diolah adalah peminjaman, dimana data yang dicatat ketika ada orang yang melakukan peminjaman adalah id, nama member, nama game, dan harga yang harus dibayar. Setiap peminjaman bisa menampilkan data hasil peminjaman dan harga yang harus dibayar. Buatlah class diagram pada studi kasus diatas! Penjelasan:
    - Harga yang harus dibayar diperoleh dari lama sewa x harga.
    - Diasumsikan 1x transaksi peminjaman game yang dipinjam hanya 1 game saja.
![1](img/tugas2.png)

    [Source program](../../src/02_Class_dan_Object/praktikum/PersewaanVideoGame.java) <br>
    [Main program](../../src/02_Class_dan_Object/praktikum/MainVideoGame.java) <br>

2. Buatlah program dari class diagram yang sudah anda buat di no 1 <br>
![2](img/tugasno2.png)

3. Buatlah program dari class diagram yang sudah anda buat di no 1! <br>
![2](img/tugas_soal1.png) <br>
![2](img/tugas3.png)

link program : <br>

[Source program Barang](../../src/02_Class_dan_Object/tugas/Lingkaran.java) <br>

4. Buatlah program sesuai dengan class diagram berikut ini: <br>
![3](img/tugas_soal2.png) <br>
Deskripsi / Penjelasan:

    - Nilai atribut hargaDasar dalam Rupiah dan atribut diskon dalam %
    - Method hitungHargaJual() digunakan untuk menghitung harga jual dengan perhitungan berikut ini: <br>
    `harga jual = harga dasar – (diskon * harga dasar)`
    - Method tampilData() digunakan untuk menampilkan nilai dari kode, namaBarang, <br>
    `hargaDasar, diskon dan harga jual`

    ![4](img/tugas4.png)

link program : <br>

[Source program Barang](../../src/02_Class_dan_Object/tugas/Barang.java) <br>

## Kesimpulan

Praktikum ini berisi tentang apa itu class dan objek dan berisi tentang attribute, method, proses instansiasi, dan try-catch selain itu di dalam praktikum ini terdapat materi tentang pemodelan class diagram dengan menggunakan UML  <br>

## Pernyataan Diri

Saya menyatakan isi tugas, kode program, dan laporan praktikum ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.

Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.

Ttd,

***Hafidh Sajid Malik***
