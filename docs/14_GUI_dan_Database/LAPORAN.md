# Laporan Praktikum #14 - GUI Dan DATABASE
## Kompetensi

Setelah menempuh materi percobaan ini, mahasiswa mampu mengenal:
1. Menggunakan paradigma berorientasi objek untuk interaksi dengan database
2. Membuat backend dan frontend
3. Membuat form sebagai frontend
  
## Ringkasan Materi

Tahapan yang akan kita lakukan adalah sebagai berikut:

1. Membuat database dan tabel-tabelnya.
2. Membuat backend yang berisi class-class yang mewakili data yang ada pada database, dan class helper untuk melakukan eksekusi query  database.
3. Membuat frontend yang merupakan antarmuka kepada pengguna. Frontend ini bisa 

berbasis teks (console), GUI, web, mobile, dan sebagainya.
Library yang digunakan untuk project ini antara lain:
1. JDBC, untuk melakukan interaksi ke database.
2. ArrayList, untuk menampung data hasil query ke database.
3. Swing, untuk membuat tampilan GUI.



## Percobaan 1 

pada praktikum ini dilakukannya pembuatan database beserta relasinya

>![1](img/1.png)



## Percobaan 2 

pada percobaan ini dilakukan penambahan library JDBC Driver untuk databse


## Percobaan 3 

pada percobaan ini dilakukan pembuatan class helper yang digunakan untuk mengeksekusi querry

Link Program Untuk Percobaan Pertama Ketiga:

>(DBHelper1841720105Hafidh) : [link ke kode program](../../src/14_GUI_dan_Database/DBHelper1841720105Hafidh.java)


## Percobaan 4 

pada percobaan ini dilakukannya pembuatan untuk menghandle CRUD pada tabel kategori


Link Program 

>(Kategori1841720105Hafidh) : [link ke kode program](../../src/14_GUI_dan_Database/Kategori1841720105Hafidh.java)



## Percobaan 5 

pada percobaan ini dilakukannya tester untuk backend dan pada table kategori

>![4](img/5.png)

Link Program 

>(TestBackend) : [link ke kode program](../../src/14_GUI_dan_Database/TestBackend1841720105Hafidh.java)



## Percobaan 6 (Frm Kategori)

Percobaan ini akan dilakukan pembuatan interface GUI pada Katgori


>(FrmKategori) : [link ke kode program](../../src/14_GUI_dan_Database/FrmKategori1841720105Hafidh.java)

>(Kategori) : [link ke kode program](../../src/14_GUI_dan_Database/Kategori1841720105Hafidh.java)


## Percobaan 7 (Frm Anggota)

Percobaan ini akan dilakukan pembuatan interface GUI pada anggota

![](img/7.png)

>(Frm Anggota) : [link ke kode program](../../src/14_GUI_dan_Database/FrmAnggota1841720105Hafidh.java)

>(Anggota) : [link ke kode program](../../src/14_GUI_dan_Database/Anggota1841720105Hafidh.java)


## Percobaan 8 (Frm Buku)
Percobaan ini akan dilakukan pembuatan interface GUI pada Buku

![](img/8.png)

>(Frm Buku) : [link ke kode program](../../src/14_GUI_dan_Database/FrmBuku1841720105Hafidh.java)

>(Buku) : [link ke kode program](../../src/14_GUI_dan_Database/Buku1841720105Hafidh.java)



## Tugas 

![6](img/tugas.png)

Link Program Untuk Tugas:


> (Form Peminjaman) : [link ke kode program](../../src/14_GUI_dan_Database/FrmPeminjaman1841720105Hafidh.java)


>(Peminjaman) : [link ke kode program](../../src/14_GUI_dan_Database/Peminjaman1841720105Hafidh.java)


## Kesimpulan

Percobaan kali ini membahas tentang cara mempelajari atau membuat aplkasi berbasis databse disertai dengan GUI


## Pernyataan Diri

Saya menyatakan isi tugas, kode program, dan laporan praktikum ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.

Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.

Ttd,

***(Hafidh Sajid M)***
