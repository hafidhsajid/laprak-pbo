package koperasigettersetter;

public class KoperasiDemo1841720105Hafidh {
    public static void main(String[] args) {
        Anggota1841720105Hafidh anggota1 = new Anggota1841720105Hafidh("Iwan","Jalan Mawar");
        System.out.println("Simpanan "+anggota1.getNamaHafidh()+" : Rp "+anggota1.getSimpananHafidh());

        anggota1.setNamaHafidh("Iwan Setiawan");
        anggota1.setAlamatHafidh("Jalan Sukarno Hatta no 10");
        anggota1.setorHafidh(100000);
        System.out.println("Simpanan "+anggota1.getNamaHafidh()+" : Rp "+anggota1.getSimpananHafidh());

        anggota1.pinjamHafidh(5000);
        System.out.println("Simpanan "+anggota1.getNamaHafidh()+" : Rp "+anggota1.getSimpananHafidh());

    }
}
