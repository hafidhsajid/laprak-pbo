package koperasigettersetter;

public class Anggota1841720105Hafidh {
    private String mNama;
    private String mAlamat;
    private float mSimpanan;

    Anggota1841720105Hafidh(String mNama, String mAlamat)
    {
        this.mNama = mNama;
        this.mAlamat = mAlamat;
        this.mSimpanan = 0;
    }

    public void setNamaHafidh(String mNama){
        this.mNama = mNama;
    }
    public void setAlamatHafidh(String mAlamat){
        this.mAlamat = mAlamat;
    }
    public String getNamaHafidh(){
        return mNama;
    }
    public String getAlamatHafidh(){
        return mAlamat;
    }
    public float getSimpananHafidh(){
        return mSimpanan;
    }
    public void setorHafidh(float mUang){
        mSimpanan += mUang;
    }
    public void pinjamHafidh(float mUang){
        mSimpanan -= mUang;
    }
}
