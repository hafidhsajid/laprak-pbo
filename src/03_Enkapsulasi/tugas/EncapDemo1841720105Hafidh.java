package tugas;

public class EncapDemo1841720105Hafidh {
    private String mName;
    private int mAge;

    public String getNameHafidh()
    {
        return mName;
    }
    public void setNameHafidh(String newName)
    {
        mName = newName;
    }
    public int getAgeHafidh()
    {
        return mAge;
    }
    public void setAgeHafidh(int newAge)
    {
        if (newAge > 30)
        {
            mAge = 30;
        }
        else if (newAge < 18){
            mAge = 18;
        }
        else
        {
            mAge = newAge;
        }
    }
}
