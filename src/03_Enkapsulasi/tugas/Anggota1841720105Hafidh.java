package tugas;

public class Anggota1841720105Hafidh {
    public String mId;
    public String mNama;
    public int mlimitPinjaman;
    public int mJumlahPinjaman;

    public Anggota1841720105Hafidh (String mId, String mNama, int mlimitPinjaman){
        this.mId = mId;
        this.mNama = mNama;
        this.mlimitPinjaman = mlimitPinjaman;
    }
    public String getNamaHafidh() {
        return mNama;
    }
    public int getGetlimitPinjamanHafidh() {
        return mlimitPinjaman;
    }
    public int getmJumlahPinjamanHafidh() {
        return mJumlahPinjaman;
    }
    public void pinjamHafidh(int mJumlah)
    {
        if (mJumlah >= mlimitPinjaman)
        {
            System.out.println("Maaf, jumlah pinjaman melebihi limit.");
        }else {
            mJumlahPinjaman += mJumlah;
        }
    }
    public void angsurHafidh(int mJumlah){
        if ((0.1*mJumlahPinjaman) >=mJumlah )
        {
            System.out.println("Maaf minimal angsuran harus 10% dari jumlah pinjaman");
        }else {
            mJumlahPinjaman -= mJumlah;
        }
    }
}
