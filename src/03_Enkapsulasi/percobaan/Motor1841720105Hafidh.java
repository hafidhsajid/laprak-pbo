package percobaan;

public class Motor1841720105Hafidh {
    private int mKecepatan = 100;
    private boolean mKontakOn = false;

    public void printStatusHafidh()
    {
        if (mKontakOn == true)
        {
            System.out.println("Kontak ON");
        }
        else
        {
            System.out.println("Kontak Off");
        }
        System.out.println("Kecepatan "+mKecepatan+"\n");
    }

    public void nyalakanMesinHafidh(){
        mKontakOn = true;
    }
    public void matikanMesinHafidh(){
        mKontakOn = false;
        mKecepatan = 0;
    }
    public void tambahKecepatanHafidh()
    {
        if (mKontakOn == true )
        {
            mKecepatan += 5;
        }else
        {
            System.out.println("Kecepatan tidak bisa bertambah karena Mesin off! \n");
        }
    }
    public void kurangiKecepatanHafidh()
    {
        if (mKontakOn == true)
        {
            mKecepatan -= 5;
        }
        else {
            System.out.println("Kecepatan tidak bisa berkurang karena Mesin off! \n");
        }
    }


}
