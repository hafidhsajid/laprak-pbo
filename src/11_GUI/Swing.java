package testing;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Swing extends javax.swing.JFrame {
    private String nama, nim, jeniskelamin, info, jurusan, matakuliah, semester;
    private JTextField namaTextField;
    private JTextField nimTextField;
    private JRadioButton lakiLakiRadioButton;
    private JRadioButton perempuanRadioButton;
    private JCheckBox sistemTerdistribusiCheckBox;
    private JCheckBox PBOCheckBox;
    private JList list1;
    private JTextArea hasil;
    private JButton clearButton;
    private JButton tampilButton;
    private JComboBox comboBox1;
    private JPanel mainPanel;


    public Swing() {
        tampilButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                nama = namaTextField.getText();
                nim = nimTextField.getText();
                if (sistemTerdistribusiCheckBox.isSelected())
                    matakuliah = "Sistem Terdistribusi";
                if (PBOCheckBox.isSelected())
                    matakuliah += "PBO";

                if (lakiLakiRadioButton.isSelected())
                    jeniskelamin = "Laki-Laki";
                if (perempuanRadioButton.isSelected())
                    jeniskelamin += "Perempuan";

                jurusan = list1.getSelectedValue().toString();
                semester = comboBox1.getSelectedItem().toString();

                info = "Nama        : "+nama+"\n";
                info += "NIM         : "+nim+"\n";
                info += "Jenis Kelamin         : "+jeniskelamin+"\n";
                info += "Jurusan         : "+jurusan+"\n";
                info += "Semester         : "+semester+"\n";
                info += "Mata Kuliah         : "+matakuliah+"\n";
                hasil.setText(info);
                JOptionPane.showMessageDialog(null, info);
            }
        });
//        tampilButton.addActionListener(new ActionListener() {

//            public void actionPerformed(ActionEvent actionEvent) {
//            }
//        });
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                namaTextField.setText("");
                nimTextField.setText("");
                sistemTerdistribusiCheckBox.setSelected(false);
                PBOCheckBox.setSelected(false);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("App");
        frame.setContentPane(new Swing().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        mainPanel = new JPanel();

    }
}
