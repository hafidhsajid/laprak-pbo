package percobaan4;

public class Kursi1841720105Hafidh {
    private String mNomer;
    private Penumpang1841720105Hafidh mPenumpang;

    public Kursi1841720105Hafidh(String mNomer) {
        this.mNomer = mNomer;
    }

    public String getmNomer() {
        return mNomer;
    }

    public void setmNomer(String mNomer) {
        this.mNomer = mNomer;
    }

    public Penumpang1841720105Hafidh getmPenumpang() {
        return mPenumpang;
    }

    public void setmPenumpang(Penumpang1841720105Hafidh mPenumpang) {
        this.mPenumpang = mPenumpang;
    }
    public String info(){
        String info = "";
        info += "Nomor  :"+mNomer+"\n";
        if (this.mPenumpang!=null){
            info+="Penumpang    : "+mPenumpang.info()+"\n";
        }return info;
    }
}
