package percobaan4;

public class Gerbong1841720105Hafidh {
    private String mKode;
    private Kursi1841720105Hafidh[] mArrayKursi;

    public Gerbong1841720105Hafidh(String mKode, int mJumlah) {
        this.mKode = mKode;
        this.mArrayKursi = new Kursi1841720105Hafidh[mJumlah];
        this.initKursi();
    }
    private void initKursi(){
        for (int i = 0; i < mArrayKursi.length; i++) {
            this.mArrayKursi[i] = new Kursi1841720105Hafidh(String.valueOf(i+1));
        }
    }

    public String getmKode() {
        return mKode;
    }

    public void setmKode(String mKode) {
        this.mKode = mKode;
    }
    public void setPenumpang(Penumpang1841720105Hafidh penumpang, int nomer){
        this.mArrayKursi[nomer - 1].setmPenumpang(penumpang);
//        Penumpang1841720105Hafidh penumpang1 = new Penumpang1841720105Hafidh(penumpang, nomer);
    }

    public Kursi1841720105Hafidh[] getmArrayKursi() {
        return mArrayKursi;
    }
    public String info(){
        String info = "";
        info += "Kode   : "+mKode+"\n";
        for (Kursi1841720105Hafidh kursi : mArrayKursi) {
            info+=kursi.info();
        }return info;
    }

}
