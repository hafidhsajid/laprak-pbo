package percobaan4;

public class Penumpang1841720105Hafidh {
    private String mKtp;
    private String mNama;

    public Penumpang1841720105Hafidh(String mKtp, String mNama) {
        this.mKtp = mKtp;
        this.mNama = mNama;
    }

    public String getmKtp() {
        return mKtp;
    }

    public void setmKtp(String mKtp) {
        this.mKtp = mKtp;
    }

    public String getmNama() {
        return mNama;
    }

    public void setmNama(String mNama) {
        this.mNama = mNama;
    }
    public String info(){
        String info = "";
        info += "KTP    :  "+mKtp+"\n";
        info += "Nama         :  "+mNama+"\n";
        return info;
    }
}
