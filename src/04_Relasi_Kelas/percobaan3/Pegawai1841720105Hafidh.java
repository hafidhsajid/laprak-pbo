package percobaan3;

public class Pegawai1841720105Hafidh {
    private String mNip;
    private String mNama;


    public Pegawai1841720105Hafidh(String mNip, String mNama) {
        this.mNip = mNip;
        this.mNama = mNama;
    }

    public String getmNip() {
        return mNip;
    }

    public void setmNip(String mNip) {
        this.mNip = mNip;
    }

    public String getmNama() {
        return mNama;
    }

    public void setmNama(String mNama) {
        this.mNama = mNama;
    }
    public String info(){ String info = "";
        info += "Nip: "+this.mNip+"\n";
        info += "Nama: "+this.mNama+"\n";
        return info;
    }

}
