package percobaan3;

public class KeretaApi1841720105Hafidh {
    private String mNama;
    private String mKelas;
    private Pegawai1841720105Hafidh mMasinis;
    private Pegawai1841720105Hafidh mAsisten;

    public KeretaApi1841720105Hafidh(String mNama, String mKelas, Pegawai1841720105Hafidh mMasinis) {
        this.mNama = mNama;
        this.mKelas = mKelas;
        this.mMasinis = mMasinis;
    }

    public KeretaApi1841720105Hafidh(String mNama, String mKelas, Pegawai1841720105Hafidh mMasinis, Pegawai1841720105Hafidh mAsisten) {
        this.mNama = mNama;
        this.mKelas = mKelas;
        this.mMasinis = mMasinis;
        this.mAsisten = mAsisten;
    }

    public String getmNama() {
        return mNama;
    }

    public void setmNama(String mNama) {
        this.mNama = mNama;
    }

    public String getmKelas() {
        return mKelas;
    }

    public void setmKelas(String mKelas) {
        this.mKelas = mKelas;
    }

    public Pegawai1841720105Hafidh getmMasinis() {
        return mMasinis;
    }

    public void setmMasinis(Pegawai1841720105Hafidh mMasinis) {
        this.mMasinis = mMasinis;
    }

    public Pegawai1841720105Hafidh getmAsisten() {
        return mAsisten;
    }

    public void setmAsisten(Pegawai1841720105Hafidh mAsisten) {
        this.mAsisten = mAsisten;
    }
    public String info(){
        String info = "";
        info +="Nama    = "+this.mNama+"\n";
        info +="Kelas   = "+this.mKelas+"\n";
        info +="Masinis = "+this.mMasinis.info()+"\n";
        info +="Asisten = "+this.mAsisten.info()+"\n";
        return info;
    }
}
