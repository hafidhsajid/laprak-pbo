package percobaan1;

public class Processor1841720105Hafidh {
    private String mMerk;
    private double mCache;

    public Processor1841720105Hafidh(String mMerk, double mCache) {
        this.mMerk = mMerk;
        this.mCache = mCache;
    }

    public Processor1841720105Hafidh() {
    }

    public String getmMerkHafidh() {
        return mMerk;
    }

    public double getmCacheHafidh() {
        return mCache;
    }

    public void setmCacheHafidh(double mCache) {
        this.mCache = mCache;
    }

    public void setmMerkHafidh(String mMerk) {
        this.mMerk = mMerk;
    }

    public void infoHafidh(){
        System.out.println("Merk Processor = "+mMerk);
        System.out.println("Cache Memory   = "+mCache);
    }

}
