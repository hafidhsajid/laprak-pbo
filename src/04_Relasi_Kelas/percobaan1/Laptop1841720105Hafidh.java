package percobaan1;

public class Laptop1841720105Hafidh {
    private String mMerk;
    private Processor1841720105Hafidh mProc;

    public Laptop1841720105Hafidh(String mMerk, Processor1841720105Hafidh mProc) {
        this.mMerk = mMerk;
        this.mProc = mProc;
    }

    public Laptop1841720105Hafidh() {
    }

    public void setmMerkHafidh(String mMerk) {
        this.mMerk = mMerk;
    }

    public void setProcHafidh(Processor1841720105Hafidh mProc) {
        this.mProc = mProc;
    }

    public void infoHafidh() {
        System.out.println("Merk Laptop    = "+mMerk);
        mProc.infoHafidh();
    }
}
