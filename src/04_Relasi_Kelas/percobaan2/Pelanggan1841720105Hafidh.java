package percobaan2;

public class Pelanggan1841720105Hafidh {
    public Pelanggan1841720105Hafidh() {

    }
    private String mNama;
    private Mobil1841720105Hafidh mMobil;
    private Sopir1841720105Hafidh mSopir;
    private int mHari;

    public String getmNama() {
        return mNama;
    }

    public void setmNama(String mNama) {
        this.mNama = mNama;
    }

    public Mobil1841720105Hafidh getmMobil() {
        return mMobil;
    }

    public void setmMobil(Mobil1841720105Hafidh mMobil) {
        this.mMobil = mMobil;
    }

    public Sopir1841720105Hafidh getmSopir() {
        return mSopir;
    }

    public void setmSopir(Sopir1841720105Hafidh mSopir) {
        this.mSopir = mSopir;
    }

    public int getmHari() {
        return mHari;
    }

    public void setmHari(int mHari) {
        this.mHari = mHari;
    }
    public int hitungBiayaTotal(){
        return mMobil.hitungmBiayaMobil(mHari)+
                mSopir.hitungmBiayaSopir(mHari);
    }
}
