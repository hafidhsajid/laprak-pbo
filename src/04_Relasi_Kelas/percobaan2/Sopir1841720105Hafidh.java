package percobaan2;

public class Sopir1841720105Hafidh {
    private int mBiaya;
    private String mNama;

    public int getmBiaya() {
        return mBiaya;
    }

    public void setmBiaya(int mBiaya) {
        this.mBiaya = mBiaya;
    }

    public String getmNama() {
        return mNama;
    }

    public void setmNama(String mNama) {
        this.mNama = mNama;
    }

    public int hitungmBiayaSopir(int mHari){
        return mBiaya*mHari;
    }
}
