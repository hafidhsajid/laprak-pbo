package percobaan2;

public class Mobil1841720105Hafidh {
    String merk;
    private int mBiaya;

    public Mobil1841720105Hafidh() {
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public int getmBiaya() {
        return mBiaya;
    }

    public void setmBiaya(int mBiaya) {
        this.mBiaya = mBiaya;
    }
    
    public int hitungmBiayaMobil(int hari){
        return mBiaya*hari;
    }
}
