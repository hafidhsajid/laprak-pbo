package testing;

public class Yours {
    private String mNama;
    private int mHP;
    private int mYourAttackingPower;
    EnemyBoss bos = new EnemyBoss();
    String statusSerang;

    public Yours() {
    }

    public Yours(String mNama, int mHP, int mAttackingPower) {
        this.mNama = mNama;
        this.mHP = mHP;
        this.mYourAttackingPower = mAttackingPower;
    }

    public int getmYourAttackingPower() {
        return mYourAttackingPower;
    }

    public String getmNama() {
        return mNama;
    }

    public void setmNama(String mNama) {
        this.mNama = mNama;
    }

    public int getmHP() {
        return mHP;
    }

    public void setmHP(int mHP) {
        this.mHP = mHP;
    }
    public int enSerangSource(int power){
        System.out.println("Kamu terkena serangan !!!!");
        return mHP-=power;
    }
    public void yourSerang(){
       bos.yourSerangSource(this.mYourAttackingPower);
    }

    public String info(){
        String info= "";
        info += "Name           = "+getmNama()+"\n";
        info += "HP             = "+getmHP()+"\n";
        info += "Atteack Power  = "+ getmYourAttackingPower()+"\n";
        return info;
    }
}
