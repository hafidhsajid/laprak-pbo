package testing;

//import tugas.Yours;

public class EnemyBoss {
    private String mNama = "EnemyBoss";
    private int mHP;
    private int mYourAttackPower;
    private int mEnAttackingPower = 25;
    Yours you = new Yours();

    public EnemyBoss() {
    }

    public EnemyBoss(int mHP, int mEnAttackPower) {
        this.mHP = mHP;
        this.mEnAttackingPower = mEnAttackPower;
    }

    public int getmEnAttackingPowerHafidh() {
        return mEnAttackingPower;
    }

    public int getmHPHafidh() {
        return mHP;
    }

    public void setmHPHafidh(int mHP) {
        this.mHP = mHP;
    }

    public int getmYourAttackPower() {
        return mYourAttackPower;
    }

    public void setmYourAttackPower(int mYourAttackPower) {
        this.mYourAttackPower = mYourAttackPower;
    }

    public String getmNama() {
        return mNama;
    }

    public void enSerang(){
        you.enSerangSource(this.mEnAttackingPower);
    }


    public void yourSerangSource(int power){
        this.mHP -= power;
        System.out.println("Kamu menyerang EnemyBoss");
//        you.statusSerang = "en";
    }

    public String info(){
        String info= "";
        info += "Monster        = "+getmNama()+"\n";
        info += "HP             = "+ getmHPHafidh()+"\n";
        info += "Atteack Power   = "+ getmYourAttackPower()+"\n";
        return info;
    }
}
