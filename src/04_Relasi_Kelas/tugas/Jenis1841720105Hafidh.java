package tugas;

public class Jenis1841720105Hafidh {
    private String mNamaJenis;
    private Monster1841720105Hafidh mMonster;

    public Jenis1841720105Hafidh(String mNamaJenis, Monster1841720105Hafidh Monster) {
        this.mNamaJenis = mNamaJenis;
        this.mMonster = Monster;
    }


    public Jenis1841720105Hafidh() {
    }
    public void setmMonster(Monster1841720105Hafidh monster){
        this.mMonster = monster;
    }

    public String infoHafidh(){
        String mInfo = "";
        mInfo += "Jenis              : "+this.mNamaJenis+"\n";
        if (this.mMonster != null)
        {
            mInfo+="     ===Monster INFO!===    \n"+mMonster.infoHafidh()+"\n";
        }
        return mInfo;
    }

}
