package tugas;

public class Monster1841720105Hafidh {
    private String mNama;
    private int mHP;
    private Loot1841720105Hafidh mLoot;


    public Monster1841720105Hafidh(String mNama, Loot1841720105Hafidh Loot) {
        this.mLoot = Loot;
        this.mNama = mNama;
        this.mHP = mHP;
    }

    public Monster1841720105Hafidh() {
    }

    public String infoHafidh(){
        String mInfo = "";
        mInfo += "Nama               : "+this.mNama+"\n";
        if (mLoot != null){
            mInfo += mLoot.infoHafidh();
        }
        return mInfo;
    }
}
