package tugas;

public class Tingkatan1841720105Hafidh {
    private int mLevel;
    private Jenis1841720105Hafidh mJenis;

    public Tingkatan1841720105Hafidh(int mLevel, Jenis1841720105Hafidh mJenis) {
        this.mLevel = mLevel;
        this.mJenis = new Jenis1841720105Hafidh();
    }
    public void setmJenis(Jenis1841720105Hafidh jenis){
        this.mJenis = jenis;
    }
    public String infoHafidh(){
        String mInfo = "";
        mInfo += "Level              : "+this.mLevel+"\n";
        if (mJenis!=null){
            mInfo+=mJenis.infoHafidh()+"\n";
        }
        return mInfo;
    }

}
