package praktikum;

public class Utama1841720105Hafidh {
    public static void main(String[] args) {
        System.out.println("Program Testing Class Manager1841720105Hafidh & Staff1841720105Hafidh");
        Manager1841720105Hafidh man[] = new Manager1841720105Hafidh[2];
        Staff1841720105Hafidh staff1[] = new Staff1841720105Hafidh[2];
        Staff1841720105Hafidh staff2[] = new Staff1841720105Hafidh[3];

        man[0]=new Manager1841720105Hafidh();
        man[0].setmNamaHafidh("Tedjo");
        man[0].setmNipHafidh("101");
        man[0].setmGolonganHafidh("1");
        man[0].setmTunjanganHafidh(500000);
        man[0].setmBagianHafidh("Administrasi");

        man[1]=new Manager1841720105Hafidh();
        man[1].setmNamaHafidh("Atika");
        man[1].setmNipHafidh("102");
        man[1].setmGolonganHafidh("1");
        man[1].setmTunjanganHafidh(250000);
        man[1].setmBagianHafidh("Pemasaran");

        staff1[0]=new Staff1841720105Hafidh();
        staff1[0].setmNamaHafidh("Usman");
        staff1[0].setmNipHafidh("0003");
        staff1[0].setmGolonganHafidh("2");
        staff1[0].setLemburHafidh(10);
        staff1[0].setGajiLemburHafidh(10000);

        staff1[1]=new Staff1841720105Hafidh();
        staff1[1].setmNamaHafidh("Anugrah");
        staff1[1].setmNipHafidh("0005");
        staff1[1].setmGolonganHafidh("2");
        staff1[1].setLemburHafidh(10);
        staff1[1].setGajiLemburHafidh(55000);
        man[0].setStaffHafidh(staff1);

        staff2[0]=new Staff1841720105Hafidh();
        staff2[0].setmNamaHafidh("Hendra");
        staff2[0].setmNipHafidh("0004");
        staff2[0].setmGolonganHafidh("3");
        staff2[0].setLemburHafidh(15);
        staff2[0].setGajiLemburHafidh(5500);

        staff2[1]=new Staff1841720105Hafidh();
        staff2[1].setmNamaHafidh("Arie");
        staff2[1].setmNipHafidh("0006");
        staff2[1].setmGolonganHafidh("4");
        staff2[1].setLemburHafidh(5);
        staff2[1].setGajiLemburHafidh(100000);

        staff2[2]=new Staff1841720105Hafidh();
        staff2[2].setmNamaHafidh("Mentari");
        staff2[2].setmNipHafidh("0007");
        staff2[2].setmGolonganHafidh("3");
        staff2[2].setLemburHafidh(6);
        staff2[2].setGajiLemburHafidh(20000);
        man[1].setStaffHafidh(staff2);

        man[0].lihatInfoHafidh();
        man[1].lihatInfoHafidh();

    }
}
