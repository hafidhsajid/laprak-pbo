package praktikum;

public class Manager1841720105Hafidh extends Karyawan1841720105Hafidh {
    private double mTunjangan;
    private String mBagian;
    private Staff1841720105Hafidh st[];

    public double getmTunjanganHafidh() {
        return mTunjangan;
    }

    public void setmTunjanganHafidh(double mTunjangan) {
        this.mTunjangan = mTunjangan;
    }

    public String getmBagianHafidh() {
        return mBagian;
    }

    public void setmBagianHafidh(String mBagian) {
        this.mBagian = mBagian;
    }

    public void setStaffHafidh(Staff1841720105Hafidh[] st) {
        this.st = st;
    }
    public void viewStaffHafidh(){
        int i;
        System.out.println("-------------------");
        for (i = 0; i < st.length; i++) {
            st[i].lihatInfoHafidh();
        }
        System.out.println("------------------");
    }
    public void lihatInfoHafidh(){
        System.out.println("Manager1841720105Hafidh     : "+this.getmBagianHafidh());
        System.out.println("NIP                         : "+this.getmNipHafidh());
        System.out.println("Nama                        : "+this.getmNamaHafidh());
        System.out.println("Gologan                     : "+this.getmGolonganHafidh());
        System.out.printf("Tunjangan                   : %.0f\n",this.getmTunjanganHafidh());
        System.out.printf("Gaji                        : %.0f\n",this.getmGajiHafidh());
        System.out.println("Bagian                      : "+this.getmBagianHafidh());
    }

    @Override
    public double getmGajiHafidh() {
        return super.getmGajiHafidh()+ mTunjangan;
    }
}
