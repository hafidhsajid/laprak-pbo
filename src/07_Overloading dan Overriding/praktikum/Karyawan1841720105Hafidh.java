package praktikum;

public class Karyawan1841720105Hafidh {
    private String mNama;
    private String mNip;
    private String mGolongan;
    private double mGaji;

    public String getmNamaHafidh() {
        return mNama;
    }

    public String getmNipHafidh() {
        return mNip;
    }

    public String getmGolonganHafidh() {
        return mGolongan;
    }

    public double getmGajiHafidh() {
        return mGaji;
    }

    public void setmNamaHafidh(String mNama) {
        this.mNama = mNama;
    }

    public void setmNipHafidh(String mNip) {
        this.mNip = mNip;
    }

    public void setmGolonganHafidh(String mGolongan) {
        this.mGolongan = mGolongan;
        switch (mGolongan.charAt(0)){
            case '1' : this.mGaji = 5000000;
            break;
            case '2' : this.mGaji = 3000000;
            break;
            case '3' : this.mGaji = 2000000;
            break;
            case '4' : this.mGaji = 1000000;
            break;
            case '5' : this.mGaji = 750000;
            break;
        }
    }

    public void setmGajiHafidh(double mGaji) {
        this.mGaji = mGaji;
    }
}
