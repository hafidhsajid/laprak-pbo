package praktikum;

public class Staff1841720105Hafidh extends Karyawan1841720105Hafidh {
    private int mLembur;
    private double mGajiLembur;

    public int getLemburHafidh() {
        return mLembur;
    }

    public void setLemburHafidh(int lembur) {
        this.mLembur = lembur;
    }

    public double getGajiLemburHafidh() {
        return mGajiLembur;
    }

    public void setGajiLemburHafidh(double gajiLembur) {
        this.mGajiLembur = gajiLembur;
    }

    public double getGajiHafidh(int lembur, double gajiLembur) {
        return super.getmGajiHafidh()+lembur*gajiLembur;
    }
    @Override
    public double getmGajiHafidh() {
        return super.getmGajiHafidh()* mGajiLembur;
    }

    public void lihatInfoHafidh(){
        System.out.println("NIP         : "+this.getmNipHafidh());
        System.out.println("Nama        : "+this.getmNamaHafidh());
        System.out.println("Gologan     : "+this.getmGolonganHafidh());
        System.out.println("Jml Lembur  : "+this.getLemburHafidh());
        System.out.printf("Gaji Lembur :%.0f\n "+this.getGajiLemburHafidh());
        System.out.printf("Gaji        :%.0f\n "+this.getmGajiHafidh());
    }
}
