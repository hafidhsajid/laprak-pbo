package tugas;

public class Tester1841720105Hafidh {
    public static void main(String[] args) {
        WalkingZombie1841720105Hafidh wz = new WalkingZombie1841720105Hafidh(100,1);
        JumpingZombie1841720105Hafidh jz = new JumpingZombie1841720105Hafidh(100,2);
        Barrier1841720105Hafidh b = new Barrier1841720105Hafidh(100);
        Plant1841720105Hafidh p = new Plant1841720105Hafidh();
        System.out.println(""+wz.getZombieInfo());
        System.out.println(""+jz.getZombieInfo());
        System.out.println(""+b.getBarrierInfo());
        System.out.println("-----------------------");

        for (int i = 0; i < 4; i++) {
            p.doDestroy(wz);
            p.doDestroy(jz);
            p.doDestroy(b);
        }
        System.out.println(""+wz.getZombieInfo());
        System.out.println(""+jz.getZombieInfo());
        System.out.println(""+b.getBarrierInfo());

    }
}
