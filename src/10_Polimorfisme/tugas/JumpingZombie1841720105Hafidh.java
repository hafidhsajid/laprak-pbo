package tugas;

public class JumpingZombie1841720105Hafidh extends Zombie1841720105Hafidh {
    public JumpingZombie1841720105Hafidh(int health, int level) {
        mHealth = health;
        mLevel = level;
    }
    public void heal(){
        if (mHealth == 1){
            super.mHealth += 0.3*super.mHealth;
        }else if (mHealth == 2){
            super.mHealth += 0.3*super.mHealth;
        }else if (mHealth ==3){
            super.mHealth += 0.4*super.mHealth;
        }
    }
    public void destroyed(){
        mHealth -= mHealth*0.093;
    }
    public String getZombieInfo(){
        String info = "";
        info += "Jumping Zombie Data = "+"\n";
        info += "Health = "+super.mHealth+"\n";
        info += "Level = "+super.mLevel+"\n";
        return info;
    }

}
