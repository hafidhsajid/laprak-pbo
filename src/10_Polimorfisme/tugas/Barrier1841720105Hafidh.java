package tugas;

public class Barrier1841720105Hafidh implements Destroyable1841720105Hafidh {
    private int mStrenght;

    public Barrier1841720105Hafidh(int mStrenght) {
        this.mStrenght = mStrenght;
    }

    public int getmStrenght() {
        return mStrenght;
    }

    public void setmStrenght(int mStrenght) {
        this.mStrenght = mStrenght;
    }

    public void destroyed(){
        mStrenght -=9;
    }

    public String getBarrierInfo(){
        String info = "";
        info += "Barrier Strength = "+mStrenght;
        return info;
    }
}
