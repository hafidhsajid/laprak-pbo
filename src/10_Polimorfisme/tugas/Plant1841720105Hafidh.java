package tugas;

public class Plant1841720105Hafidh {
    public void doDestroy(Destroyable1841720105Hafidh d) {
        if (d instanceof WalkingZombie1841720105Hafidh){
            ((WalkingZombie1841720105Hafidh)d).destroyed();
        }else if (d instanceof JumpingZombie1841720105Hafidh){
            ((JumpingZombie1841720105Hafidh)d).destroyed();
        }else if (d instanceof Barrier1841720105Hafidh){
            ((Barrier1841720105Hafidh)d).destroyed();
        }
    }
}
