package tugas;

public class WalkingZombie1841720105Hafidh extends Zombie1841720105Hafidh {
    public WalkingZombie1841720105Hafidh(int health, int level){
        mHealth = health;
        mLevel = level;
    };
    void heal(){
        if (mHealth == 1){
            mHealth = (int) (mHealth+(mHealth*20/100));
        }else if (mHealth == 2){
            super.mHealth += 0.3*super.mHealth;
        }else if (mHealth ==3){
            super.mHealth += 0.4*super.mHealth;
        }
    };
    public void destroyed(){
        mHealth -= 0.2*mHealth;
    };
    public String getZombieInfo(){
        String info = "";
        info += "Walking Zombie Data = "+"\n";
        info += "Health = "+super.mHealth+"\n";
        info += "Level = "+super.mLevel+"\n";
        return info;
    };
}
