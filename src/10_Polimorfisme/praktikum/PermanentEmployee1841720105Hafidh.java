package praktikum;

public class PermanentEmployee1841720105Hafidh extends Employee1841720105Hafidh implements Payable1841720105Hafidh {
    private int salary;

    public PermanentEmployee1841720105Hafidh(String name, int salary) {
        this.mName = name;
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public int getPaymentAmountHafidh() {
        return (int) (salary*0.05*salary);
    }

    @Override
    public String getEmployeeInfoHafidh() {
        String info = super.getEmployeeInfoHafidh()+"\n";
        info += "Registered as permanent employee with salary "+salary+"\n";
        return info;
    }
}
