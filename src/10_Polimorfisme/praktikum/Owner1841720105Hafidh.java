package praktikum;

public class Owner1841720105Hafidh {
    public void payHafidh(Payable1841720105Hafidh p){
        System.out.println("Total payment = "+p.getPaymentAmountHafidh());
        if (p instanceof ElectricityBill1841720105Hafidh){
            ElectricityBill1841720105Hafidh eb =(ElectricityBill1841720105Hafidh) p;
            System.out.println(""+eb.getBillInfoHafidh());
        }else if (p instanceof PermanentEmployee1841720105Hafidh){
            PermanentEmployee1841720105Hafidh pe = (PermanentEmployee1841720105Hafidh) p;
            pe.getEmployeeInfoHafidh();
            System.out.println(""+pe.getEmployeeInfoHafidh());
        }
    }
    public void showMyEmployeeHafidh(Employee1841720105Hafidh e){
        System.out.println(""+e.getEmployeeInfoHafidh());
        if (e instanceof PermanentEmployee1841720105Hafidh)
            System.out.println("You have to pay her/him monthly!!");
        else System.out.println("No need to pay him/her :)");
    }
}
