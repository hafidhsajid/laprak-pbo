package praktikum;

public class ElectricityBill1841720105Hafidh implements Payable1841720105Hafidh {
    private  int kwh;
    private  String category;

    public ElectricityBill1841720105Hafidh(int kwh, String category) {
        this.kwh = kwh;
        this.category = category;
    }

    public int getKwhHafidh() {
        return kwh;
    }

    public void setKwhHafidh(int kwh) {
        this.kwh = kwh;
    }

    public String getCategoryHafidh() {
        return category;
    }

    public void setCategoryHafidh(String category) {
        this.category = category;
    }

    @Override
    public int getPaymentAmountHafidh() {
        return kwh+ getBasePriceHafidh();
    }
    public int getBasePriceHafidh(){
        int bPrice = 0;
        switch (category){
            case "R-1" : bPrice = 100;break;
            case "R-2" : bPrice = 200;break;
        }return bPrice;
    }
    public String getBillInfoHafidh(){
        return "KWH = "+kwh+"\n"+ "Category = "+category+"("+ getBasePriceHafidh()+"per KWH)\n";
    }

}
