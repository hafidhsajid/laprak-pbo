package praktikum;

public class IntershipEmployee1841720105Hafidh extends Employee1841720105Hafidh {
    private int length;

    public IntershipEmployee1841720105Hafidh(String name, int length) {
        this.length = length;
        this.mName = name;
    }

    public int getLengthHafidh() {
        return length;
    }

    public void setLengthHafidh(int length) {
        this.length = length;
    }

    @Override
    public String getEmployeeInfoHafidh() {
        String info = super.getEmployeeInfoHafidh()+"\n";
        info += "Registered as internship employee for"+length+" month/s\n";
        return info;
    }
}
