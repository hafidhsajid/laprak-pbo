package praktikum.praktikum2.interfacelatihan;

public class PascaSarjana1841720105Hafidh extends Mahasiswa1841720105Hafidh implements ICumlaude1841720105Hafidh {
    public PascaSarjana1841720105Hafidh(String nama) {
        super(nama);
    }

    @Override
    public void lulus() {
        System.out.println("Aku sudah menyelesaikan TESIS");

    }

    @Override
    public void meraihIPKTinggi() {
        System.out.println("IPK-ku lebih dari 3,71");

    }
}
