package praktikum.praktikum2.interfacelatihan;

public class Sarjana1841720105Hafidh extends Mahasiswa1841720105Hafidh implements ICumlaude1841720105Hafidh {
    public Sarjana1841720105Hafidh(String nama) {
        super(nama);
    }

    @Override
    public void lulus() {
        System.out.println("Aku sudah menyelesaikan SKRIPSI");
    }

    @Override
    public void meraihIPKTinggi() {
        System.out.println("IPK-ku lebih dari 3,51");

    }
}
