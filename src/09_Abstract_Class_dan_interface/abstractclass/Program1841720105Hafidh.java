package praktikum.praktikum1.abstractclass;

public class Program1841720105Hafidh {
    public static void main(String[] args) {
        Kucing1841720105Hafidh kucingKampung = new Kucing1841720105Hafidh();
        Ikan1841720105Hafidh lumbaLumba = new Ikan1841720105Hafidh();

        Orang1841720105Hafidh ani = new Orang1841720105Hafidh("Ani");
        Orang1841720105Hafidh budi = new Orang1841720105Hafidh("Budi");

        ani.peliharaanHewan(kucingKampung);
        budi.peliharaanHewan(lumbaLumba);

        ani.ajakPeliharaanJalnJalan();
        budi.ajakPeliharaanJalnJalan();
    }
}
