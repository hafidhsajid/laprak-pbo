package praktikum.praktikum1.abstractclass;

public abstract class Hewan1841720105Hafidh {
    private int umur;
    protected Hewan1841720105Hafidh(){
        this.umur = 0;
    }
    public void bertambahUmur(){
        this.umur += 1;
    }
    public abstract void bergerak();
}

