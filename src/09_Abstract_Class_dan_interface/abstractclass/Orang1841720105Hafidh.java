package praktikum.praktikum1.abstractclass;

public class Orang1841720105Hafidh {
    private String mNama;
    private Hewan1841720105Hafidh mHewanPeliharaan;

    public Orang1841720105Hafidh(String mNama) {
        this.mNama = mNama;
    }

    public void peliharaanHewan(Hewan1841720105Hafidh hewanPeliharaan) {
        this.mHewanPeliharaan = hewanPeliharaan;
    }
    public void ajakPeliharaanJalnJalan(){
        System.out.println("Namaku "+this.mNama);
        System.out.println("Hewan peliharanku berjalan dengan cara: ");
        this.mHewanPeliharaan.bergerak();
        System.out.println("---------------------------------------");
    }
}
