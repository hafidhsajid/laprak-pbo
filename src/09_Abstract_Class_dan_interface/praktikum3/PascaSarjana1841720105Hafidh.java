package praktikum.praktikum3;

public class PascaSarjana1841720105Hafidh extends Mahasiswa1841720105Hafidh implements ICumlaude1841720105Hafidh, IBerprestasi {
    public PascaSarjana1841720105Hafidh(String nama) {
        super(nama);
    }

    @Override
    public void menjaraiKompetisi() {
        System.out.println("Saya menjuarai kompetisi INTERNASIONAL");

    }

    @Override
    public void membuatPublikasiIlmiah() {
        System.out.println("Saya menerbitkan artikel di jurnal INTERNASIONAL");

    }

    @Override
    public void lulus() {
        System.out.println("Aku sudah menyelesaikan TESIS");

    }

    @Override
    public void meraihIPKTinggi() {
        System.out.println("IPK-ku lebih dari 3,71");

    }
}
