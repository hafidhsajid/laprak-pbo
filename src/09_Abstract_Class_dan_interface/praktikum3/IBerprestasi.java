package praktikum.praktikum3;

public interface IBerprestasi {
    public abstract void menjaraiKompetisi();
    public abstract void membuatPublikasiIlmiah();
}
