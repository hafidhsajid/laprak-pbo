package praktikum.praktikum3;

public class Rektor1841720105Hafidh {
    public void beriSertifikatCumlaude(ICumlaude1841720105Hafidh mahasiswa){
        System.out.println("Saya REKTOR, memberikan sertifikat cumlaude.");
        System.out.println("Selamat! Bagaimana Anda bisa cumlaude?");

//        mahasiswa.KuliahDiKampus();

        mahasiswa.lulus();
        mahasiswa.meraihIPKTinggi();


        System.out.println("=========================================");
    }
    public void beriSertifikatMawapres(IBerprestasi mahasiswa){
        System.out.println("Saya REKTOR, memberikan sertifikat MAWAPRES.");
        System.out.println("Selamat! Bagaimana Anda bisa berprestasi?");

        mahasiswa.menjaraiKompetisi();
        mahasiswa.membuatPublikasiIlmiah();
        System.out.println("=========================================");
    }

}
