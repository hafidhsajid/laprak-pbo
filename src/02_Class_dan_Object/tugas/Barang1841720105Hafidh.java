package tugas;

public class Barang1841720105Hafidh {
    public String mKode;
    public String mNamaBarang;
    public int mHargaDasar;
    public float mDiskon;

    private int hitungHargaJual(){
        float mDisc = mDiskon/100;
        double mHitung = mDisc * mHargaDasar;
        double mHasil = mHargaDasar - mHitung;
        int mResult = (int) mHasil;
        return mResult;
    }
    public void tampilData(){
        System.out.println("mKode\t : "+mKode);
        System.out.println("Nama barang\t : "+ mNamaBarang);
        System.out.println("Harga dasar\t : Rp. "+mHargaDasar);
        System.out.println("Harga jual\t : Rp. "+hitungHargaJual());
    }
}
class main1841720105Hafidh {
    public static void main(String[] args) {
        Barang1841720105Hafidh source = new Barang1841720105Hafidh();
        source.mKode = "New Stuff123";
        source.mNamaBarang = "Barang baru";
        source.mHargaDasar = 10000;
        source.mDiskon = 10;
        source.tampilData();
    }
}
