package tugas;

public class Lingkaran1841720105Hafidh {
    private double mPhi = Math.PI;
    public double mR;

    public double hitungmLuas(double mR){
        double mRadius = Math.pow(mR,2);
        double mLuas = mPhi*mRadius;
        System.out.println("Hasil dari penghitungan mLuas : "+mLuas);
        return mLuas;
    }
    public double hitungmKeliling(double mR){
        double mKeliling = 2*mPhi*mR;
        System.out.println("Hasil dari penghitungan mKeliling : "+mKeliling);
        return mKeliling;
    }
}
class Main1841720105Hafidh {

    public static void main(String[] args) {
        Lingkaran1841720105Hafidh ling = new Lingkaran1841720105Hafidh();
        ling.hitungmLuas(7);
        ling.hitungmKeliling(10);
    }
}
