package praktikum;

public class Barang1841720105Hafidh {
    public String mNamaBrg;
    public String mJenisBrg;
    public int mStok;

    public void tampilBarangHafidh(){
        System.out.println("Nama Barang \t: "+mNamaBrg);
        System.out.println("Jenis Barang \t: "+mJenisBrg);
        System.out.println("Stok \t\t\t: "+mStok);
    }
//    method with return value
    public int tambahStokHafidh(int brgMasuk){
        int stokBaru = brgMasuk + mStok;
        return stokBaru;
    }
}
