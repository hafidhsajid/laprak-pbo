package tugas;

public class Laptop1841720105Hafidh extends Komputer1841720105Hafidh {
    public String jnsBaterai;
    public Laptop1841720105Hafidh() {
    }

    public Laptop1841720105Hafidh(String merk, int kecProcessor, int sizeMemory, String jnsProcessor, String jnsBaterai) {
        super(merk, kecProcessor, sizeMemory, jnsProcessor);
        this.jnsBaterai = jnsBaterai;
    }

    public void tampilLaptop(){
        super.tampilData();
        System.out.println("Jenis Baterai          = "+jnsBaterai);
    }
}
