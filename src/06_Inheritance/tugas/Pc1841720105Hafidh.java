package tugas;

public class Pc1841720105Hafidh extends Komputer1841720105Hafidh {
    public int ukuranMonitor;
    public Pc1841720105Hafidh() {
    }

    public Pc1841720105Hafidh(String merk, int kecProcessor, int sizeMemory, String jnsProcessor, int ukuranMonitor) {
        super(merk, kecProcessor, sizeMemory, jnsProcessor);
        this.ukuranMonitor = ukuranMonitor;
    }

    public void tampilPc(){
        super.tampilData();
        System.out.println("Ukuran Monitor         = "+ukuranMonitor);
    }

}
