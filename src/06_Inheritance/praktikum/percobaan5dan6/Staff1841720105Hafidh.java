package praktikum.percobaan5dan6;

public class Staff1841720105Hafidh extends Karyawan1841720105Hafidh {
    public int lembur, potongan;

    public Staff1841720105Hafidh() {
    }

    public Staff1841720105Hafidh(String nama, String alamat, String jk, int umur, int gaji, int lembur, int potongan) {
        super(nama, alamat, jk, umur, gaji);
        this.lembur = lembur;
        this.potongan = potongan;
    }
    public void tampilDataStaffHafidh(){
        super.tampilDataKaryawanHafidh();
        System.out.println("Lembur              = "+lembur);
        System.out.println("Potongan            = "+potongan);
        System.out.println("Total Gaji          = "+(gaji+lembur+potongan));
    }
}
