package praktikum.percobaan5dan6;

public class Inheritance1_1841720105Hafidh {
    public static void main(String[] args) {
////        Percobaan 5
//        Manager1841720105Hafidh M = new Manager1841720105Hafidh();
//        M.nama = "Vivin";
//        M.alamat = "Jl. Vinolia";
//        M.umur = 25;
//        M.jk = "Perempuan";
//        M.gaji = 3000000;
//        M.tunjangan =  1000000;
//        M.tampilDataManagerHafidh();
//
//        Staff1841720105Hafidh S = new Staff1841720105Hafidh();
//        S.nama = "Lestari";
//        S.alamat = "Malang";
//        S.umur = 25;
//        S.jk = "Perempuan";
//        S.gaji = 2000000;
//        S.lembur = 500000;
//        S.potongan = 250000;
//        S.tampilDataStaffHafidh();

////        Percobaan 6
        StaffTetap1841720105Hafidh ST = new StaffTetap1841720105Hafidh("Budi", "Malang", "Laki-Laki", 20, 2000000, 250000, 200000, "2A", 100000);
        ST.tampilStaffTetapHafidh();

        StaffHarian1841720105Hafidh SH = new StaffHarian1841720105Hafidh("Indah", "Malang", "Perempuan", 27, 10000, 100000, 50000, 100);
        SH.tampilStaffHarianHafidh();
    }
}
