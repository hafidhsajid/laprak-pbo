package praktikum.percobaan5dan6;

public class StaffHarian1841720105Hafidh extends Staff1841720105Hafidh {
    public int jmlJamKerja;
    public StaffHarian1841720105Hafidh(){

    }

    public StaffHarian1841720105Hafidh(String nama, String alamat, String jk, int umur, int gaji, int lembur, int potongan, int jmlJamKerja) {
        super(nama, alamat, jk, umur, gaji, lembur, potongan);
        this.jmlJamKerja = jmlJamKerja;
    }
    public void tampilStaffHarianHafidh(){
        System.out.println("========Data Staff Harian==========");
        super.tampilDataStaffHafidh();
        System.out.println("Jumlah Jam Kerja    = "+jmlJamKerja);
        System.out.println("Gaji Bersih         = "+(gaji*jmlJamKerja+lembur-potongan));
    }
}
