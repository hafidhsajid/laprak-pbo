package praktikum.percobaan5dan6;

public class StaffTetap1841720105Hafidh extends Staff1841720105Hafidh {
    public String golongan;
    public int asuransi;

    public StaffTetap1841720105Hafidh(){

    }

    public StaffTetap1841720105Hafidh(String nama, String alamat, String jk, int umur, int gaji, int lembur, int potongan, String golongan, int asuransi) {
        super(nama, alamat, jk, umur, gaji, lembur, potongan);
        this.golongan = golongan;
        this.asuransi = asuransi;
    }
    public void tampilStaffTetapHafidh(){
        System.out.println("========Data Staff Tetap==========");
        super.tampilDataStaffHafidh();
        System.out.println("Golongan            = "+golongan);
        System.out.println("Jumlah Asuransi     = "+asuransi);
        System.out.println("Gaji Bersih         = "+(gaji+lembur-potongan-asuransi));
    }
}
