package praktikum.percobaan2;

public class ClassA1841720105Hafidh {
    private int x;
    private int y;

    public void setXHafidh(int x){
        this.x = x;
    }
    public void setYHafidh(int y){
        this.y = y;
    }

    public int getXHafidh() {
        return x;
    }

    public int getYHafidh() {
        return y;
    }

    public void getNilaiHafidh(){
        System.out.println("Nilai x:"+x);
        System.out.println("Nilai y:"+y);

    }
}
